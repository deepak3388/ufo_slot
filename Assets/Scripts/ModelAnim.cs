﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelAnim : MonoBehaviour
{
    static ModelAnim intance_;

    [System.Serializable]
    public class Model
    {
        public GameObject model_;
        public int num_;
        public string type_;
    }

    public Model[] models;


    void OnEnable()
    {
        intance_ = this;
    }

    public static void ShowAnim(int num)
    {
        GameObject obj = null;
        for (int i=0;i < intance_.models.Length;i++)
        {
            if (intance_.models[i].num_ == num)
            {
                obj = intance_.models[i].model_;
                break;
            }
        }

        if (obj != null)
        {
            obj.SetActive(true);            
        }
        else
        {
            obj = intance_.models[2].model_;
            obj.SetActive(true);
        }
        Delayed.Function(() =>
        {
            obj.SetActive(false);
        }, 4.0f);
    }



}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace TripleChance
{
    public class Wheels : MonoBehaviour
    {
        public static Wheels instance;

        public static event System.Action RotationStarted;
        public static event System.Action RotationFinished;
//        public static event System.Action UpdateWheelNumber;
        public static event System.Action HistoryUpdated;
        public static event System.Action<string[]> OnGotLastTenBet;

        public static List<int> wheel1NumbersHistory = new List<int>();
        public static List<int> wheel2NumbersHistory = new List<int>();
        public static List<int> wheel3NumbersHistory = new List<int>();

		public List<float> wheel1Angle = new List<float> ();
		public List<float> wheel2Angle = new List<float> ();
		public List<float> wheel3Angle = new List<float> ();

		public static bool canSpin = true;

        public static void Spin()
        {
            instance.RotateChildren();
        }

        public static void UpdateNumbersHistory(string number, bool spin)
        {
            int[] numberdigits = new int[3];
            for (int i = 0; i < numberdigits.Length; i++)
//            for (int i = numberdigits.Length-1; i >= 0; i--)
            {
                try
                {
                    numberdigits[numberdigits.Length - i - 1] = int.Parse(number[i].ToString());
                }
                catch { }
            }
            UpdateNumbersHistory(numberdigits[0], numberdigits[1], numberdigits[2], spin);
        }
        public static void UpdateNumbersHistory(int newWheel1No, int newWheel2No, int newWheel3No, bool spin)
        {
//            Debug.Log("newWheel1No : " +newWheel1No+ "newWheel2No : " +newWheel2No+ "newWheel3No : " +newWheel3No);
            wheel1NumbersHistory.Insert(0, newWheel1No);
            if (wheel1NumbersHistory.Count > 10)
                wheel1NumbersHistory.RemoveAt(9);

            wheel2NumbersHistory.Insert(0, newWheel2No);
            if (wheel2NumbersHistory.Count > 10)
                wheel2NumbersHistory.RemoveAt(9);

            wheel3NumbersHistory.Insert(0, newWheel3No);
            if (wheel3NumbersHistory.Count > 10)
                wheel3NumbersHistory.RemoveAt(9);


            if(spin) instance.RotateParents(newWheel1No, newWheel2No, newWheel3No);
        }

        public Transform wheel1, wheel2, wheel3;
        public AnimationCurve rotationCurve = AnimationCurve.Linear(0, 0, 1, 1);
        Transform wheel1Child, wheel2Child, wheel3Child;

        public Transform dummyWheel1 , dummyWheel2 , dummyWheel3;

        [HideInInspector]public float totalRotationTime1_ =13;       // change from 9
        [HideInInspector]public float totalRotationTime2_ =12;           // change from 8
        [HideInInspector]public float totalRotationTime3_ =11;        // change from 7
        float elapsedRotationTime1 = 0;
        float elapsedRotationTime2 = 0;
        float elapsedRotationTime3 = 0;
        float wheel1TargetAngle = 0;
        float wheel2TargetAngle = 0;
        float wheel3TargetAngle = 0;

		float dummyWheel1TargetAngle = 0;
		float dummyWheel2TargetAngle = 0;
		float dummyWheel3TargetAngle = 0;

        void Awake()
        {

            instance = this;
            wheel1Child = wheel1.GetChild(0);
            wheel2Child = wheel2.GetChild(0);
            wheel3Child = wheel3.GetChild(0); 
        }

        void OnEnable()
        {
            //GameplayServerEvents.Connected += GetLastWinNumbers;
            //GameSession.Started += GetLastWinNumbers;
            //GameplayServerEvents.gotWinnerNumber += RotateParentChildren;
        } 

        void OnDisable()
        {
            //GameplayServerEvents.Connected -= GetLastWinNumbers;
            //GameSession.Started -= GetLastWinNumbers;
            //GameplayServerEvents.gotWinnerNumber -= RotateParentChildren;

        }


        void GetLastWinNumbers()
        {
            //GameHandlers.Gameplay.LastTenWinNumbers(GameStatics.TripleChanceGameId, OnGetWinNumbers, null);
        }

        public void OnGetWinNumbers()
        {
//            wheel1NumbersHistory.Clear();
//            wheel2NumbersHistory.Clear();
//            wheel3NumbersHistory.Clear();

//            string[] numbers = dataObj.GetUtfStringArray("winNumbers");

//            if (numbers!=null && numbers.Length>0)
//            {
//                for (int i = numbers.Length-1; i >= 0; i--)
////                for (int i = 0; i < numbers.Length; i++)
//                {
//                    UpdateNumbersHistory(numbers[i],  i == 0);
//                }
//            }

//            OnGotLastTenBet(dataObj.GetUtfStringArray("winNumbers"));

//            if (HistoryUpdated != null)
//                HistoryUpdated();
        } 

        void RotateChildren()
        {

			StopCoroutine("RotateChildren1_c");
			StartCoroutine("RotateChildren1_c");
			StopCoroutine("RotateChildren2_c");
			StartCoroutine("RotateChildren2_c");
			StopCoroutine("RotateChildren3_c");
			StartCoroutine("RotateChildren3_c");

//            AudioPlayer.PlaySFX("spinStart");
        }

        public void RotateParentChildren(string winnerNumber){
			
            // added by kapil----

            Debug.Log("got winner number Rotating parent---");

        }


        IEnumerator RotateChildren1_c()
        { 

            // added by kapil----
            // to show number before---- 

//            Delayed.Function(()=>{
//                Debug.Log("setting for wheel number!!!");
//                if(UpdateWheelNumber != null){
//                    UpdateWheelNumber();
//                }
//            } , 2.0f);

//            Delayed.Function(()=>{
//                Debug.Log("going to set numbers!!!!");
//                if (RotationFinished != null){
//                    RotationFinished();
//                }
//            } , 2.0);


            if (RotationStarted != null)
            {
                RotationStarted();  
            }
            elapsedRotationTime1 = 0;

//        float r = ((int)Random.Range(totalRotationTime1/2, totalRotationTime1)) * -360;
            float r = ((int)totalRotationTime1_) * -360;
            float rt = 0;     
            float lf = 0;

            float lta = 0;

			Debug.Log ("totalRotationTime1 : " +totalRotationTime1_);


            while (elapsedRotationTime1 < totalRotationTime1_)
            {
//				Debug.Log ("inside ----------------");
                lf = rotationCurve.Evaluate(elapsedRotationTime1 / totalRotationTime1_); 
                rt = Mathf.Lerp(0, r, lf); 
                wheel1Child.localEulerAngles = new Vector3(0, 0, rt % 360); 
                yield return null;
                elapsedRotationTime1 += Time.deltaTime; 
                if (Mathf.Abs(wheel1Child.localEulerAngles.z - lta) > 20)
                {
                    //AudioPlayer.PlaySFX("spinStart");
//					Debug.Log ("inside if----------------");
//                    AudioPlayer.PlaySFX("tick");
                    lta = wheel1Child.localEulerAngles.z; 
                }
            } 
            wheel1Child.localEulerAngles = Vector3.zero;
            yield return new WaitForSeconds(0.5f);
            if(RotationFinished != null){
                RotationFinished();
            }

//            AudioPlayer.PlaySFX("spinEnd", .2f);
        }

        IEnumerator RotateChildren2_c()
        {  
            elapsedRotationTime2 = 0;

//        float r = ((int)Random.Range(totalRotationTime2/2, totalRotationTime2)) * 360;
            float r = ((int)totalRotationTime2_) * 360;
            float rt = 0;     
            float lf = 0;

            float lta = 0;

            while (elapsedRotationTime2 < totalRotationTime2_)
            {
                lf = rotationCurve.Evaluate(elapsedRotationTime2 / totalRotationTime2_); 
                rt = Mathf.Lerp(0, r, lf); 
                wheel2Child.localEulerAngles = new Vector3(0, 0, rt % 360); 
                yield return null;
                elapsedRotationTime2 += Time.deltaTime; 
                if (Mathf.Abs(wheel1Child.localEulerAngles.z - lta) > 20)
                {
                    //AudioPlayer.PlaySFX("spinStart");
//                    AudioPlayer.PlaySFX("tick");
                    lta = wheel1Child.localEulerAngles.z; 
                }
            } 
            wheel2Child.localEulerAngles = Vector3.zero; 
			//AudioPlayer.instance.musicSource.volume = 0.05f;
   //         AudioPlayer.PlaySFX("wheelStop");
			//Delayed.Function (() => {
			//	AudioPlayer.instance.musicSource.volume = 0.1f;
			//}, 1.0f);


        }

        IEnumerator RotateChildren3_c()
        {   
            elapsedRotationTime3 = 0;

//        float r = ((int)Random.Range(totalRotationTime3/2, totalRotationTime3)) * -360;
            float r = ((int)totalRotationTime3_) * -360;
            float rt = 0;     
            float lf = 0;

            float lta = 0;


			Debug.Log ("totalRotationTime3 : " +totalRotationTime3_);
            while (elapsedRotationTime3 < totalRotationTime3_)
            {
                lf = rotationCurve.Evaluate(elapsedRotationTime3 / totalRotationTime3_); 
                rt = Mathf.Lerp(0, r, lf); 
                wheel3Child.localEulerAngles = new Vector3(0, 0, rt % 360); 
                yield return null;
                elapsedRotationTime3 += Time.deltaTime; 
                if (Mathf.Abs(wheel1Child.localEulerAngles.z - lta) > 20)
                {
                    //AudioPlayer.PlaySFX("spinStart");
//                    AudioPlayer.PlaySFX("tick");
                    lta = wheel1Child.localEulerAngles.z; 
                }
            } 
            wheel3Child.localEulerAngles = Vector3.zero; 

			//AudioPlayer.instance.musicSource.volume = 0.05f;

			//AudioPlayer.PlaySFX("wheelStop");
			//Delayed.Function (() => {
			//	AudioPlayer.instance.musicSource.volume = 0.1f;
			//}, 1.0f);
        }


        public void RotateParents(int wheel1No, int wheel2No, int wheel3No)
        {
			// old work ----
//            wheel1TargetAngle = wheel1No * 36;
//            wheel2TargetAngle = wheel2No * 36;
//            wheel3TargetAngle = wheel3No * 36;
			// till here --- 

			// new work -- 
			Debug.Log("wheel1No: " +wheel1No);
			Debug.Log("wheel2No: " +wheel2No);
			Debug.Log("wheel3No: " +wheel3No);

			wheel1TargetAngle = wheel1Angle[wheel1No];
			wheel2TargetAngle = wheel2Angle[wheel2No];
			wheel3TargetAngle = wheel3Angle[wheel3No];
            StopCoroutine("RotateParents_c");
            StartCoroutine("RotateParents_c");     // change by kapil---- commented
        }


        IEnumerator RotateParents_c()
        {
            float t = totalRotationTime3_ - elapsedRotationTime3 - 1;
//            Debug.Log(t);
            if (t < 0) t = 0;
            float te = 0;
            float r1t = 0, r2t = 0, r3t = 0;
            float lf = 0;

            while (te < t)
            {
//                lf = rotationCurve.Evaluate(elapsedRotationTime3 / totalRotationTime3);
                lf = rotationCurve.Evaluate(te / t);
                r1t = Mathf.Lerp(360, wheel1TargetAngle, lf);
                r2t = Mathf.Lerp(360, wheel2TargetAngle, lf);
                r3t = Mathf.Lerp(360, wheel3TargetAngle, lf);

                wheel1.localEulerAngles = new Vector3(0, 0, r1t % 360);
                wheel2.localEulerAngles = new Vector3(0, 0, r2t % 360);
                wheel3.localEulerAngles = new Vector3(0, 0, r3t % 360);
            
                yield return null;
                te += Time.deltaTime;
            }

//            Debug.Log("wheel1TargetAngle : " +wheel1TargetAngle);
//            Debug.Log("wheel2TargetAngle : " +wheel2TargetAngle);
//            Debug.Log("wheel2TargetAngle : " +wheel3TargetAngle);


            wheel1.localEulerAngles = new Vector3(0, 0, wheel1TargetAngle);
            wheel2.localEulerAngles = new Vector3(0, 0, wheel2TargetAngle);
            wheel3.localEulerAngles = new Vector3(0, 0, wheel3TargetAngle);

           
        }



        public Vector3 testVals;


        void TestParentRotation()
        {
            UpdateNumbersHistory((int)testVals.x, (int)testVals.y, (int)testVals.z, true);
        }

    }
}
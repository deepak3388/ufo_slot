﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Machine : MonoBehaviour {

    public Animator animator_;

    public GameObject animObject;

    public BigWin bigWinObject;
    public Win winObject;
    public SuperMegaWin superMegaWinObject;

    public GameObject freeGameObject;

    public Symbol symbolObject;

    [System.Serializable]
    public class LoadedMatrix1Card
    {
        public GameObject card;
    }

    public LoadedMatrix1Card[] matrix1Cards;

    [System.Serializable]
    public class LoadedMatrix2Card
    {
        public GameObject card;
    }
    public LoadedMatrix1Card[] matrix2Cards;

    [System.Serializable]
    public class LoadedMatrix3Card
    {
        public GameObject card;
    }

    public LoadedMatrix1Card[] matrix3Cards;


    void OnEnable()
    {
        MatrixWheel.OnStopWheelRotation += OnRotationFinished;
    }

    void OnDisable()
    {
        MatrixWheel.OnStopWheelRotation -= OnRotationFinished;
    }

    void OnRotationFinished()
    {
        if (GameLogic.isWinSetted)
        {
            if (GameLogic.winSymbol > 0)
            {
                if (animObject != null)
                {
                    ModelAnim.ShowAnim(GameLogic.winSymbol);
                    //animObject.SetActive(true);
                    //animator_.SetInteger("setAnim", GameLogic.winSymbol);
                }
            }
            
            StopCoroutine("ShowWin");
            StartCoroutine("ShowWin");
        }
        else
        {
            Gameui.OnGameReset();
        }
        GameLogic.isWinSetted = false;
    }

    IEnumerator ShowWin()
    {
        if (GameLogic.winSymbol > 0)
        {
            yield return new WaitForSeconds(4.0f);
            GameLogic.winSymbol = -1;
        }  

        
        if (animObject.activeSelf)
            animObject.SetActive(false);

        print("GameLogic.winAmount : " + GameLogic.winAmount);
        if (GameLogic.winAmount > 0)
        {
            int myBetAmount = GameLogic.myTotalBet;
            int bigWinAmount = myBetAmount * 5;
            int megaWinAmount = myBetAmount * 10;
            Gameui.SetWinAmount(GameLogic.winAmount);
            if (GameLogic.winAmount >= bigWinAmount && GameLogic.winAmount < megaWinAmount)
            {
                bigWinObject.gameObject.SetActive(true);
                bigWinObject.OnShowBigWon(GameLogic.winAmount);

                yield return new WaitForSeconds(2.0f);
            }
            else if (GameLogic.winAmount >= megaWinAmount)
            {
                superMegaWinObject.gameObject.SetActive(true);
                superMegaWinObject.OnMegaBigWon(GameLogic.winAmount);
                yield return new WaitForSeconds(2.0f);
            }
            else
            {
                winObject.gameObject.SetActive(true);
                winObject.OnShowWin(GameLogic.winAmount);
                yield return new WaitForSeconds(2.0f);
            }

            superMegaWinObject.gameObject.SetActive(false);
            winObject.gameObject.SetActive(false);
            bigWinObject.gameObject.SetActive(false);
        }

        print("Gameui.autoSpin "+ Gameui.autoSpin);
        print("GameLogic.freeSpinLeft " + GameLogic.freeSpinLeft);
        if (Gameui.autoSpin > 0)
        {
            yield return new WaitForSeconds(1.0f);
            Gameui.SetMessage("AutoSpin");
            Gameui.SetTotalBet(GameLogic.myTotalBet);
            Gameui.autoSpin--;
            GameLogic.CreateWheel(Gameui.betAmount);
            GameLogic.winAmount = 0;
            //GameLogic.
        }
        else if (GameLogic.freeSpinLeft > 0) {
            freeGameObject.SetActive(true);
            yield return new WaitForSeconds(2.0f);
            freeGameObject.SetActive(false);
            Gameui.SetMessage("Free Game");
            print("my total bet : "+ GameLogic.myTotalBet);
            Gameui.SetTotalBet(GameLogic.myTotalBet);
            GameLogic.freeSpinLeft--;
            print("free spin left : " + GameLogic.freeSpinLeft);
            GameLogic.OnCheckFreeSpin();
        }
        else
        {
            Gameui.OnGameReset();
            Gameui.SetMessage("Bet and Spin");
            GameLogic.winAmount = 0;
            Gameui.SetTotalBet(0);
        }
        

    }



}

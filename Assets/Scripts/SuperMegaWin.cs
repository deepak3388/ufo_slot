﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuperMegaWin : MonoBehaviour {

    public Transform megaWinObject;
    public Text megaWinAmountText;

    public void OnMegaBigWon(int amount_)
    {
        megaWinAmountText.text = "" + amount_;
        if (megaWinObject != null)
            megaWinObject.gameObject.SetActive(true);
    }
}

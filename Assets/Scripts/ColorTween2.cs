using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;

public class ColorTween2 : MonoBehaviour {
    public MaskableGraphic target;
    public MaskableGraphic[] additionalTargets;
    MaskableGraphic targetGraphic
    {
        get
        {
            if (target == null)
            {
                target = GetComponent<MaskableGraphic>();
            }
            return target;
        }
    }
    //public Color startColor = Color.white, endColor = Color.white;
    //public AnimationCurve curve = new AnimationCurve(new Keyframe[] { new Keyframe(0, 0), new Keyframe(1, 1) });

	public Gradient gradient;
    public bool alphaOnly = false; 

    public float initialDleay = 0;
    public float duration = 1;

    public UnityEvent onEnd;

    public bool loop;
    public bool startOnEnable = true;

    float timeElapsed;

    void OnEnable()
    {
       if(startOnEnable) StartTween();
    }

    void OnDisable()
    {
        EndTween();
    }

    public void StartTween() {
        StartCoroutine("Tween");
    }

    public void EndTween() {
        StopCoroutine("Tween");
    }

    IEnumerator Tween()
    {
        if (initialDleay > 0) yield return new WaitForSeconds(initialDleay);
		if (duration > 0) { 
			timeElapsed = 0;
			while (timeElapsed < duration) {
				ApplyColor (gradient.Evaluate (timeElapsed / duration));
				timeElapsed += Time.deltaTime;
				yield return null;
			}
		}
		ApplyColor (gradient.Evaluate (1));
        onEnd.Invoke();
        if (duration > 0 && loop) StartCoroutine("Tween");
    }

	void ApplyColor(Color col){
		if (alphaOnly) col = new Color (targetGraphic.color.r, targetGraphic.color.g, targetGraphic.color.b, col.a);
		targetGraphic.color = col;
		for (int i = 0; i < additionalTargets.Length; i++) {
			if (alphaOnly) col = new Color (additionalTargets [i].color.r, additionalTargets [i].color.g, additionalTargets [i].color.b, col.a);
			additionalTargets [i].color = col;
		}
	}
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CardItemList : MonoBehaviour {

    public int matrixNum;

    public Image cardImage;

    public GameObject indicator;

    Sprite cardSprite_;
    public Sprite cardSprite
    {
        set
        {
            cardSprite_ = value;
            if (cardImage != null)
                cardImage.sprite = cardSprite_;
        }
    }

}

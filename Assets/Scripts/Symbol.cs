﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Symbol : MonoBehaviour {

    [System.Serializable]
    public class LoadSymbol
    {
        public string matrixName;
        [System.Serializable]
        public class LoadSymbolSprite
        {
            public int keyNum;
            public string name;
            public GameObject symbolSprite;
        }
        public LoadSymbolSprite[] loadedSymbol;
    }

    public LoadSymbol[] matrixSymbol;

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Gameui : MonoBehaviour {

    static Gameui instance_;

    public Button spinButton;
    public Button betButton;
    public Button autoSpintButton;
    public Button maxButton;
    public Button infoButton;
    public Button settingButton;
    public Button soundButton;
    public Button fullScreenButton;

    public GameObject betDropDown;
    bool isBetActive;


    public GameObject autoSpinDropDown;
    bool isAutoSpinActive;

    public Text totalBetText;
    public Text creditText;
    public Text winText;
    public Text messageText;

    public int maxBet;

    static int betAmount_;
    public static int betAmount
    {
        get
        {
            return betAmount_;
        }

        set
        {
            betAmount_ = value;
            instance_.betDropDown.SetActive(false);
            instance_.isBetActive = false;
        }
    }

    static int autoSpin_;
    public static int autoSpin
    {
        get{return autoSpin_; }

        set{autoSpin_ = value;}
    }

    Interpolate.Position posAnim;

    void OnEnable()
    {
        MatrixWheel.OnStartWheelRotation += OnWheelRotationStarted;

        instance_ = this;
        betAmount = 0;
        isBetActive = false;
        if (infoButton != null)
            infoButton.onClick.AddListener(OnInfoClick);

        if (settingButton != null)
            settingButton.onClick.AddListener(OnSettingButtonClick);

        if (spinButton != null)
            spinButton.onClick.AddListener(OnSpin);

        if (betButton != null)
            betButton.onClick.AddListener(OnBetButton);

        if (autoSpintButton != null)
            autoSpintButton.onClick.AddListener(OnAutoSpinButton);

        if (maxButton != null)
            maxButton.onClick.AddListener(OnMaxButton);

        SetMessage("Bet and Spin");
    }

    void OnDisable()
    {
        if (settingButton != null)
            settingButton.onClick.AddListener(OnSettingButtonClick);

        if (infoButton != null)
            infoButton.onClick.RemoveListener(OnInfoClick);

        if (spinButton != null)
            spinButton.onClick.RemoveListener(OnSpin);

        if (betButton != null)
            betButton.onClick.RemoveListener(OnBetButton);

        if (autoSpintButton != null)
            autoSpintButton.onClick.RemoveListener(OnAutoSpinButton);

        if (maxButton != null)
            maxButton.onClick.RemoveListener(OnMaxButton);
    }
        
    void OnInfoClick()
    {

    }

    void OnSettingButtonClick()
    {

    }

    void OnSoundButtonClick()
    {

    }

    void OnSpin()
    {
        if (betAmount > 0)
        {
            SetMessage("Good Luck");
            GameLogic.CreateWheel(betAmount);
            betAmount = 0;
        }
        else
        {
            SetMessage("Place your bets!");
        }
    }

    public static void OnAutoSpinSelected()
    {
        if(instance_.autoSpinDropDown != null)
            instance_.autoSpinDropDown.SetActive(false);

        instance_.OnSpin();
    }
    

    void OnBetButton()
    {
        print("clicked!!");
        isBetActive = !isBetActive;
        betDropDown.SetActive(isBetActive);
        if (isBetActive)
        {
            posAnim = new Interpolate.Position(betDropDown.transform, new Vector3(-112f, -151f, 0f), new Vector3(-112f, -6f, 0f), 0.5f, true);
        }
        
    }

    void OnAutoSpinButton()
    {
        isAutoSpinActive = !isAutoSpinActive;
        autoSpinDropDown.SetActive(isAutoSpinActive);
        if (isAutoSpinActive)
        {
            posAnim = new Interpolate.Position(autoSpinDropDown.transform, new Vector3(111f, -151f, 0f), new Vector3(111f, -3f, 0f), 0.5f, true);
        }
    }

    void OnMaxButton()
    {
        if (maxBet == 0)
        {
            return;
        }
        betAmount = maxBet;
        SetTotalBet(maxBet * 500);
    }

    public static void SetMessage(string message_)
    {
        if (instance_.messageText != null)
        {
            instance_.messageText.text = message_;
        }
    }

    void OnWheelRotationStarted()
    {
        if (spinButton != null)
            spinButton.interactable = false;
        if (betButton != null)
            betButton.interactable = false;
        if (autoSpintButton != null)
            autoSpintButton.interactable = false;
        if (maxButton != null)
            maxButton.interactable = false;
        if (infoButton != null)
            infoButton.interactable = false;
        if (settingButton != null)
            settingButton.interactable = false;
        if (soundButton != null)
            soundButton.interactable = false;
        if (fullScreenButton != null)
            fullScreenButton.interactable = false;
    }

    public static void OnGameReset()
    {
        if (instance_.spinButton != null)
            instance_.spinButton.interactable = true;
        if (instance_.betButton != null)
            instance_.betButton.interactable = true;
        if (instance_.autoSpintButton != null)
            instance_.autoSpintButton.interactable = true;
        if (instance_.maxButton != null)
            instance_.maxButton.interactable = true;
        if (instance_.infoButton != null)
            instance_.infoButton.interactable = true;
        if (instance_.settingButton != null)
            instance_.settingButton.interactable = true;
        if (instance_.soundButton != null)
            instance_.soundButton.interactable = true;
        if (instance_.fullScreenButton != null)
            instance_.fullScreenButton.interactable = true;
    }

    public static void SetTotalBet(int amount_)
    {
        if (amount_ == 0)
        {
            instance_.totalBetText.text = "";
            return;
        }
        instance_.totalBetText.text =  "" +amount_;
    }

    public static void SetWinAmount(int amount_)
    {
        if (amount_ == 0)
        {
            instance_.winText.text = "";
            return;
        }
        instance_.winText.text = "" + amount_;
    }

    public void OnExit()
    {
        Application.Quit();
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BigWin : MonoBehaviour {

    public Transform bigWinObject;
    public Text bigWinAmountText;

    void OnEnable()
    {
    }

    void OnDisable()
    {

    }

    public void OnShowBigWon(int amount_)
    {
        bigWinAmountText.text = "" + amount_;

        if (bigWinObject != null)
            bigWinObject.gameObject.SetActive(true);
    }

    public void StopBigWin()
    {
        if (bigWinObject != null)
            bigWinObject.gameObject.SetActive(false);
    }

}

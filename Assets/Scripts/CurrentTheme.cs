﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CurrentTheme : MonoBehaviour {

    static CurrentTheme instance;

    [System.Serializable]
    public class LoadData
    {
        public string name;
        public string cardType;
        public string desrciption;
    }


    public string currentTheme;

    public GameObject splash;

    public Transform matrixParent1;
    public Transform matrixParent2;
    public Transform matrixParent3;

    public Symbol symbolObject;
    int count = 1;

    public static bool generate = true;

    void OnEnable()
    {
        instance = this;
    }

    void OnActivateSplash()
    {
        splash.SetActive(true);
    }

    public static void CreateWheel(string count_, List<int> matList)
    {
        if (instance.count > 3)
        {
            return;
        }

        Transform parent = null;
        List<Symbol.LoadSymbol.LoadSymbolSprite> loadedSymbol = new List<Symbol.LoadSymbol.LoadSymbolSprite>();
        loadedSymbol.Clear();
        //for (int j = 0; j < symbolObject.matrixSymbol.Length; i++)
        //{
        //    if (symbolObject.matrixSymbol[i].matrixName == count_)
        //    {
        //        loadedSymbol.AddRange(symbolObject.matrixSymbol[i].loadedSymbol);
        //        break;
        //    }
        //}
        //for (int i=0;i< matList.Count;i++)
        //{

        //}


        for (int i = 0; i < instance.symbolObject.matrixSymbol.Length; i++)
        {
            if (instance.symbolObject.matrixSymbol[i].matrixName == count_)
            {
                loadedSymbol.AddRange(instance.symbolObject.matrixSymbol[i].loadedSymbol);
                break;
            }
        }
        switch (count_)
        {
            case "1":
                {
                    parent = instance.matrixParent1;
                    break;
                }
            case "2":
                {
                    parent = instance.matrixParent2;
                    break;
                }

            case "3":
                {
                    parent = instance.matrixParent3;
                    break;
                }
        }

        instance.GenerateItem(parent , loadedSymbol , matList);
    }
    void GenerateItem(Transform parent_ , List<Symbol.LoadSymbol.LoadSymbolSprite> symbol_ , List<int> matList_ )
    {
        if (generate)
        {
            for (int i = 0; i < matList_.Count; i++)
            {
                for (int j = 0; j < symbol_.Count; j++)
                {
                    if (matList_[i] == symbol_[j].keyNum)
                    {
                        GameObject go = Instantiate(symbol_[j].symbolSprite);
                        go.transform.SetParent(parent_.transform.GetChild(i), false);
                        go.transform.localPosition = Vector3.zero;
                        go.SetActive(true);
                        break;
                    }
                }
            }
        }
        else
        {

        }

    }

}

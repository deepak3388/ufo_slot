﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SplashLoader : MonoBehaviour {

    public GameObject panel;
    public Slider slider;

    public Button playButton;
    public Text percentText;


    void OnEnable()
    {

        Delayed.Function(()=>
        {
            SpaceShipAnim.OnSpaceAnim();
        } , 2.0f);

        return;
        if (!panel.activeSelf)
            panel.SetActive(true);
        playButton.onClick.AddListener(OnPlayButton);

        StopCoroutine("LoadGame");
        StartCoroutine("LoadGame");
    }

    void OnDisable()
    {
        playButton.onClick.RemoveListener(OnPlayButton);
    }

    void OnPlayButton()
    {
        if(panel.activeSelf)
            panel.SetActive(false);

        
    }

    IEnumerator LoadGame()
    {
        float t = 0;
        float totalTime = 5f;
        percentText.text = "Loading...";
        while ( totalTime > 1)
        {
            totalTime -= Time.deltaTime;
            slider.value = 1 / totalTime;

            yield return null;
        }
        if(!playButton.gameObject.activeSelf)
            playButton.gameObject.SetActive(true);

        percentText.text = "Completed";


    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Win : MonoBehaviour {

    public Transform winObject;
    public Text winAmountText;

    public void OnShowWin(int amount_)
    {
        winAmountText.text = "" + amount_;

        if (winObject != null)
            winObject.gameObject.SetActive(true);
    }

    public void StopWin()
    {
        if (winObject != null)
            winObject.gameObject.SetActive(false);
    }



}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class AutoSpinItemList : MonoBehaviour
{
    public int autoSpin;
    public Button itemButton;
    void OnEnable()
    {
        if (itemButton != null)
        {
            itemButton.onClick.AddListener(OnButtonClick);
        }
    }

    void OnDisable()
    {
        if (itemButton != null)
        {
            itemButton.onClick.RemoveListener(OnButtonClick);
        }
    }

    void OnButtonClick()
    {
        Gameui.autoSpin = this.autoSpin;
        Gameui.OnAutoSpinSelected();
    }

}

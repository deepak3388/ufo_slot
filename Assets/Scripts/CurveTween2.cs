using UnityEngine;
using System.Collections;
using UnityEngine.Events;

public class CurveTween2 : MonoBehaviour
{
	public enum Scope
	{
		Position,
		Eulers,
		Scale

	}

	public enum Space
	{
		Local,
		Global

	}

	[System.Serializable]
	public class KeyPoint
	{
		public Vector3 valAbsolute;
		public Transform valReference;

		public Vector3 getValue(Scope sc, Space sp)
		{
			if (valReference == null)
			{
				return valAbsolute;
			}
			else
			{
				switch (sc)
				{
					case Scope.Position:
						if (sp == Space.Local)
							return valReference.localPosition;
						else
							return valReference.position;

					case Scope.Eulers:
						if (sp == Space.Local)
							return valReference.localEulerAngles;
						else
							return valReference.eulerAngles; 

					case Scope.Scale:
						return valReference.localScale; 

					default:
						return valAbsolute;
				}
			}
		}
	}


	public Transform target;

	public Transform targetTransform
	{
		get
		{
			if (target == null)
			{
				target = transform;
			}
			return target;
		}
	}

	public Scope scope;
	public Space space;

	public AnimationCurve xCurve = AnimationCurve.Linear(0, 0, 1, 1);
	public AnimationCurve yCurve = AnimationCurve.Linear(0, 0, 1, 1);
	public AnimationCurve zCurve = AnimationCurve.Linear(0, 0, 1, 1);

	public KeyPoint startPoint, endPoint;

	public float initialDelay = 0;
	public float loopDelay = 0;
	public float duration = 1;

	public UnityEvent onStart;
	public UnityEvent onEnd;

	public bool loop;
	public bool startOnEnable = true;

	private bool hasLooped = false;

	float realTimeLastFrame, timeElapsed, x, y, z;

	void OnEnable()
	{
		hasLooped = false;
		if (startOnEnable)
			StartTween();
	}

	void OnDisable()
	{
		EndTween();
	}

	public void StartTween()
	{
		StartCoroutine("Tween");
	}

	public void EndTween()
	{
		StopCoroutine("Tween");
	}

	IEnumerator Tween()
	{
		onStart.Invoke();

		timeElapsed = 0;
		ApplyVal();
		if(loop && hasLooped && loopDelay > 0)
			yield return new WaitForSeconds(loopDelay);
		else if (initialDelay > 0)
			yield return new WaitForSeconds(initialDelay);
		while (timeElapsed < duration)
		{
			timeElapsed += Time.deltaTime;
			ApplyVal();
			yield return null;
		}

		ApplyVal();
		onEnd.Invoke();
		if (loop)
		{
			hasLooped = true;
			StartCoroutine("Tween");
		}
	}

	void ApplyVal()
	{
		x = Mathf.LerpUnclamped(startPoint.getValue(scope, space).x, endPoint.getValue(scope, space).x, xCurve.Evaluate(timeElapsed / duration));
		y = Mathf.LerpUnclamped(startPoint.getValue(scope, space).y, endPoint.getValue(scope, space).y, yCurve.Evaluate(timeElapsed / duration));
		z = Mathf.LerpUnclamped(startPoint.getValue(scope, space).z, endPoint.getValue(scope, space).z, zCurve.Evaluate(timeElapsed / duration));
		switch (scope)
		{
			case Scope.Position:
				if (space == Space.Local)
					targetTransform.localPosition = new Vector3(x, y, z);
				else
					targetTransform.position = new Vector3(x, y, z);
				break;
			case Scope.Eulers:
				if (space == Space.Local)
					targetTransform.localEulerAngles = new Vector3(x, y, z);
				else
					targetTransform.eulerAngles = new Vector3(x, y, z);
				break;
			case Scope.Scale:
				targetTransform.localScale = new Vector3(x, y, z);
				break;
			default:
				break;
		} 
	}
    
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameLogic : MonoBehaviour
{
    static GameLogic instance_;

    int[][] baseReel = new int[][]
    {
        new int[]{6,7,8,11,10,9,3,6,11,8,12,11,10,7,4,11,6,7,13,2,9,10,11,8,7,6,1,11,0,5,6,7,1,11,5,0,9,11,4,6,8,5,2,0,4,6,2,8,1,3,11,13,4,1,11,10,5,8,11,9,1,10,13,11,7,6,9,12,10,4,9,8,12,11,5,6,7,12,11,9,4,2,11,10,3,5,13,11,0,2,9,10,3,11,7,0,6,7,8,3,13,10,11,12,8,9,10,11,13,8,9,10,11,7,13},
        new int[]{6,13,8,11,0,9,7,6,1,8,11,6,10,7,11,12,4,7,10,11,13,10,5,11,7,6,1,11,0,5,6,7,11,5,4, 10, 9, 11, 4, 6, 11, 13, 2, 11, 4, 3, 2, 8, 1, 3, 11, 2, 4, 1, 8, 10, 5, 11, 0, 9, 8, 10, 9, 2, 7, 6, 13, 11, 10, 12, 9, 8, 11, 1, 5, 6, 7, 12, 11, 9, 4, 3, 12, 10, 6, 5, 8, 11, 0, 7, 9, 10, 3, 11, 2, 0, 6, 7, 13, 3, 9, 10, 11, 12, 8, 9, 13, 11, 7, 8, 9, 10, 11, 13, 8 },
        new int[]{6,7,8,11,5,13,3,0,11,8,5,6,10,13,11,1,0,7,10,11,9,13,11,4,7,6,9,11,3,5,6,7,11,5,4,7,13,11,12,6,8,11,2,0,10,6,7,13,1,3,10,2,4,1, 8, 10, 5, 2, 11, 9, 1, 10, 9, 4, 7, 6, 8, 11, 10, 4, 6, 8, 11, 9, 5, 6, 7, 12, 11, 9, 4, 2, 11, 10, 3, 5, 8, 11, 12, 1, 9, 12, 3, 11, 2, 0, 6, 7, 8, 11, 9, 10, 11, 12, 13, 9, 10, 11, 7, 8, 9, 10, 11, 13, 8 }
    };

    int[][] freeReel = new int[][]
    {
        new int[]{6,7,8,11,10,9,3,6,11,8,0,11,10,7,4,11,6,7,10,2,9,10,11,8,7,6,1,11,0,5,6,7,1,11,5,0,9,11,4,6,8,5,2,0,4,6,2,8,1,3,11,7,4,1,11,10,5,8,11,9,1,10,9,11,7,6,9,5,10,4,9,8,0,11,5,6,7,5,11,9,4,2,11,10,3,6,8,11,0,2,9,10,3,11,7,0,6,7,8,3,9,10,11,5,8,9,10,11,7,8,9,10,11,7,8},
        new int[]{6,7,8,11,0,9,7,6,1,8,11,6,10,7,11,5,4,7,10,11,9,10,5,11,7,6,1,11,0,5,6,7,11,5,4,10,9,11,4,6,11,8,2,11,4,3,2,8,1,3,11,2,4,1,8,10,5,11,0,9,8,10,9,2,7,6,9,11,10,5,9,8,11,1,5,6,7,3,11,9,4,3,0,10,6,5,8,11,0,7,9,10,3,11,2,0,6,7,8,3,9,10,11,0,8,9,10,11,7,8,9,10,11,7,8},
        new int[]{6,7,8,11,5,9,3,0,11,8,5,6,10,7,11,1,0,7,10,11,9,10,11,4,7,6,9,11,3,5,6,7,11,5,4,7,9,11,0,6,8,11,2,0,10,6,7,8,1,3,10,2,4,1,8,10,5,2,11,9,1,10,9,4,7,6,8,11,10,4,6,8,11,9,5,6,7,4,11,9,4,2,11,10,3,5,8,11,5,1,9,0,3,11,2,0,6,7,8,11,9,10,11,0,8,9,10,11,7,8,9,10,11,7,8}
    };

    int[] scatterFreegameTable = new int[] { 0, 8, 15 };
    int scatterSymbol = 12;
    int bonusSymbol = 13;
    int wild1 = 0;
    int minBet = 500;
    int bet = 0;

    public static int myTotalBet = 0;

    int[][] matrix;

    public List<int> matrix1 = new List<int>();
    public List<int> matrix2 = new List<int>();
    public List<int> matrix3 = new List<int>();

    public static int cardCount = -1;
    public static string winType = string.Empty;
    public static int winSymbol = -1;
    public static int winAmount = 0;

    public static bool isWinSetted = false;
     int[] reelStops_ = new int[] { 6 , 114 , 80};

    public List<int> reelStop = new List<int>();

    public static int totalFreeSpins = 0;
    public static int freeSpinLeft = 0;

    void OnEnable()
    {
        instance_ = this;
    }

    void Start()
    {
        GenerateBaseWheelMatrix(false);
    }

    void GenerateBaseWheelMatrix(bool isCreate)
    {
        reelStop.Clear();
        for (int i = 0; i < instance_.reelStops_.Length ; i++)
        {
            instance_.reelStops_[i] = Random.Range(0, baseReel[i].Length);
            reelStop.Add(instance_.reelStops_[i]);
        }
        int count = 0;
        int[][] mainMatrix = CreateMatrix(baseReel, instance_.reelStops_);
        matrix = mainMatrix;
        matrix1.Clear();
        matrix2.Clear();
        matrix3.Clear();
        for (int i = 0; i < mainMatrix.Length; i++)
        {
            for (int j = 0; j < mainMatrix[i].Length; j++)
            {
                if (i == 0)
                {
                    matrix1.Add(mainMatrix[i][j]);
                }
                if (i == 1)
                {
                    matrix2.Add(mainMatrix[i][j]);
                }
                if (i == 2)
                {
                    matrix3.Add(mainMatrix[i][j]);
                }
            }
        }
        CurrentTheme.CreateWheel("" + 1, matrix1);
        CurrentTheme.CreateWheel("" + 2, matrix2);
        CurrentTheme.CreateWheel("" + 3, matrix3);
        //CurrentTheme.generate = false;
        if(isCreate)
        GetBaseGameSpin();
    }

    void GenerateFreeSpin(bool isCreate)
    {
        for (int i = 0; i < instance_.reelStops_.Length; i++)
        {
            instance_.reelStops_[i] = Random.Range(0, freeReel[i].Length);
        }
        int count = 0;
        int[][] mainMatrix = CreateMatrix(freeReel, instance_.reelStops_);
        matrix = mainMatrix;
        matrix1.Clear();
        matrix2.Clear();
        matrix3.Clear();
        for (int i = 0; i < mainMatrix.Length; i++)
        {
            for (int j = 0; j < mainMatrix[i].Length; j++)
            {
                if (i == 0)
                {
                    matrix1.Add(mainMatrix[i][j]);
                }
                if (i == 1)
                {
                    matrix2.Add(mainMatrix[i][j]);
                }
                if (i == 2)
                {
                    matrix3.Add(mainMatrix[i][j]);
                }
            }
        }

        CurrentTheme.CreateWheel("" + 1, matrix1);
        CurrentTheme.CreateWheel("" + 2, matrix2);
        CurrentTheme.CreateWheel("" + 3, matrix3);
        if (isCreate)
            FreeSpin();
    }

    static int betAmount = 0;
    public static void CreateWheel(int amount_)
    {
        betAmount = amount_;
        //Delayed.Function(()=>
        //{

        //} , 3.0f);
        instance_.StartCoroutine("CreateWheel_");
    }

    IEnumerator CreateWheel_()
    {
        instance_.Spin();
        yield return new WaitForSeconds(3.0f);
        instance_.GenerateBaseWheelMatrix(true);
    }

    void Spin()
    {
        MatrixWheel.RotateChildren();
    }

    public static void GetBaseGameSpin()
    {
        print(" -------------- ");
        LoadGameSettingData gameSettingobj = instance_.baseGameSpin(betAmount);
        //print("totalFreeSpins : " + gameSettingobj.gameDataObject.totalFreeSpins);
        //print("freeSpinsLeft : " + gameSettingobj.gameDataObject.freeSpinsLeft);
        //print("accumulatedLineWin : " + gameSettingobj.gameDataObject.accumulatedLineWin);
        //print("freegameScatterWin : " + gameSettingobj.gameDataObject.freegameScatterWin);
        //print("accumulatedFreeGameWin :  " + gameSettingobj.gameDataObject.accumulatedFreeGameWin);
        //print("accumulatedGameWin " + gameSettingobj.gameDataObject.accumulatedGameWin);
        //print("freeSpinSessionsLeft : " + gameSettingobj.gameDataObject.freeSpinSessionsLeft);
        totalFreeSpins = gameSettingobj.gameDataObject.totalFreeSpins;
        freeSpinLeft = gameSettingobj.gameDataObject.freeSpinsLeft;
        isWinSetted = false;
        winAmount = 0;
        int[][] winMat = gameSettingobj.outcomeData.matrix;
        for (int i=0;i< gameSettingobj.outcomeData.win.Count;i++)
        {
            cardCount = gameSettingobj.outcomeData.win[i].count;
            winAmount += gameSettingobj.outcomeData.win[i].amount;
            winType = gameSettingobj.outcomeData.win[i].winType;
            winSymbol = gameSettingobj.outcomeData.win[i].symbol;
            //print("cardCount :  " + gameSettingobj.outcomeData.win[i].count);
            //print("winAmount " + gameSettingobj.outcomeData.win[i].amount);
            //print("winType " + gameSettingobj.outcomeData.win[i].winType);
            //print("winSymbol " + winSymbol);
            isWinSetted = true;
        }

        //for (int i = 0; i < winMat.Length; i++)
        //{
        //    for (int j = 0; j < winMat[i].Length; j++)
        //    {
        //        print("i : " +i+ " j : "+j+" num : "+ winMat[i][j]);
        //    }
        //}

        print("freeGame : " + gameSettingobj.outcomeData.freeGame);
        print("bonus : " + gameSettingobj.outcomeData.bonus);
    }

    public static void OnCheckFreeSpin()
    {
        instance_.Spin();
        instance_.StartCoroutine("CheckFreeSpin");
        //Delayed.Function(()=>
        //{
            
        //} , 3.0f);
    }

    IEnumerator CheckFreeSpin()
    {
        yield return new WaitForSeconds(3.0f);
        instance_.GenerateFreeSpin(true);
    }

    void FreeSpin()
    {
        LoadGameSettingData freeGamedataObject = instance_.freeGameSpin(betAmount, instance_.reelStops_);
        //print("accumulatedLineWin" + freeGamedataObject.gameDataObject.accumulatedLineWin);
        //print("accumulatedLineWin" + freeGamedataObject.outcomeData.type);
        winAmount = 0;
        for (int i = 0; i < freeGamedataObject.outcomeData.win.Count; i++)
        {
            winAmount += freeGamedataObject.outcomeData.win[i].amount;
            //print("winType : " + freeGamedataObject.outcomeData.win[i].winType);
            //print("amount : " + freeGamedataObject.outcomeData.win[i].amount);
            //print("count : " + freeGamedataObject.outcomeData.win[i].count);
            //print("symbol : " + freeGamedataObject.outcomeData.win[i].symbol);
            isWinSetted = true;
        }
    }



    //Function to return base game matrix and win
    //LoadGameSettingData baseGameSpin(int _betAmount, int[] _reelsStops)
    LoadGameSettingData baseGameSpin(int _betAmount)
    {
        print("_betAmount : " + _betAmount);
        bet = _betAmount * minBet;
        myTotalBet = bet;
        //totalBet = bet * minBet;
        //////////////Creating baseGame outcomes/////////////
        int totalFreeSpinsBaseGame = 0;
        string gameMode = "baseGame";

        List<LoadReturnData> winsFound = checkWin("baseGame", matrix);
        ////////End of Checking the BaseGame trigger///////
        ///
        ///////Calculating the anticipating columns for bonus////////
        int[] bonusAnticipationDataInBaseGame = new int[] { 0, 0, 0 };
        int[] bonusSymbolsCount = new int[] { 0, 0, 0 };

        for (int i = 0; i < matrix.Length; i++)
        {
            for (int j = 0; j < matrix[i].Length; j++)
            {
                if (matrix[i][j] == bonusSymbol)
                {
                    bonusSymbolsCount[i]++;
                }
            }
        }


        //for (int col = 0; col < matrix[0].Length - 1; col++)
        //{
        //    for (int row = 0; row < matrix.Length; row++)
        //    {
        //        print("col : " + col + " row "+ row);
        //        if (matrix[row][col] == bonusSymbol)
        //        {
        //            print(" bonusSymbolsCount " + bonusSymbolsCount[col]);
        //            bonusSymbolsCount[col]++;
        //        }
        //    }
        //}

        if (bonusSymbolsCount[0] >= 1)
        {
            bonusAnticipationDataInBaseGame[1] = 1;
        }

        if (bonusSymbolsCount[0] + bonusSymbolsCount[1] >= 1)
        {
            bonusAnticipationDataInBaseGame[2] = 1;
        }

        ////End of Calculating the anticipating columns for bonus////

        ///////Calculating the anticipating columns for scatter////////
        int[] scatterAnticipationDataInBaseGame = new int[] { 0, 0, 0 };
        int[] scatterSymbolsCount = new int[] { 0, 0, 0 };

        for (int i = 0; i < matrix.Length; i++)
        {
            for (int j = 0; j < matrix[i].Length; j++)
            {
                if (matrix[i][j] == scatterSymbol)
                {
                    scatterSymbolsCount[i]++;
                }
            }
        }



        //for (int col = 0; col < matrix[0].Length - 1; col++)
        //{
        //    for (int row = 0; row < matrix.Length; row++)
        //    {
        //        if (matrix[row][col] == scatterSymbol)
        //        {
        //            scatterSymbolsCount[col]++;
        //        }
        //    }
        //}

        if (scatterSymbolsCount[0] >= 1)
        {
            scatterAnticipationDataInBaseGame[1] = 1;
        }

        if (scatterSymbolsCount[0] + scatterSymbolsCount[1] >= 1)
        {
            scatterAnticipationDataInBaseGame[2] = 1;
        }

        ////End of Calculating the anticipating columns for scatter////

        ///////////Checking the bonus trigger///////////
        bool bonusDataInBaseGame;
        List<LoadCheckBonusData> bonusTrigger = checkForBonusFound(matrix);
        if (bonusTrigger.Count > 0)
        {
            bonusDataInBaseGame = true;
        }
        else
        {
            bonusDataInBaseGame = false;
        }
        ////////End of Checking the bonus trigger///////

        ///////////Checking the freeGame trigger///////////
        bool freeGameDataInBaseGame;
        List<LoadFreeSpinData> freeGameTrigger = checkForFreeSpinFound(matrix);
        if (freeGameTrigger.Count > 0)
        {
            LoadReturnData returnDataObject = new LoadReturnData();
            returnDataObject.winType = freeGameTrigger[0].winType;
            returnDataObject.symbol = freeGameTrigger[0].symbol;
            returnDataObject.count = freeGameTrigger[0].count;
            returnDataObject.amount = freeGameTrigger[0].amount;
            winsFound.Add(returnDataObject);

            totalFreeSpinsBaseGame = scatterFreegameTable[freeGameTrigger[0].count - 1];
            freeGameDataInBaseGame = true;
        }
        else
        {
            freeGameDataInBaseGame = false;
        }
        ////////End of Checking the freeGame trigger///////
        int tempTotalWinInSpin = 0;
        int freegameScatterWin = 0;
        for (int i = 0; i < winsFound.Count; i++)
        {
            if (winsFound[i].winType == "payLine")
            {
                tempTotalWinInSpin += winsFound[i].amount;
            }
            else if (winsFound[i].winType == "scatter")
            {
                freegameScatterWin += winsFound[i].amount;
            }
        }
        LoadGameData gamedataObject = new LoadGameData();
        gamedataObject.totalFreeSpins = totalFreeSpinsBaseGame;
        gamedataObject.freeSpinsLeft = totalFreeSpinsBaseGame;
        gamedataObject.accumulatedLineWin = tempTotalWinInSpin;
        gamedataObject.freegameScatterWin = freegameScatterWin;
        gamedataObject.accumulatedFreeGameWin = 0;
        gamedataObject.accumulatedGameWin = 0;
        gamedataObject.freeSpinSessionsLeft = 0;

        LoadOutcomeData outcomeDataObject = new LoadOutcomeData();
        outcomeDataObject.type = gameMode;
        outcomeDataObject.matrix = matrix;
        outcomeDataObject.win = winsFound;
        outcomeDataObject.freeGame = freeGameDataInBaseGame;
        outcomeDataObject.freeGameData = new int[] { };
        outcomeDataObject.bonus = bonusDataInBaseGame;

        LoadGameSettingData gameSettingsObject = new LoadGameSettingData();
        gameSettingsObject.gameDataObject = gamedataObject;
        gameSettingsObject.outcomeData = outcomeDataObject;
        return gameSettingsObject;
    }

    public class LoadGameData
    {
        public int totalFreeSpins;
        public int freeSpinsLeft;
        public int accumulatedLineWin;
        public int freegameScatterWin;
        public int accumulatedFreeGameWin;
        public int accumulatedGameWin;
        public int freeSpinSessionsLeft;
    }

    public class LoadOutcomeData
    {
        public string type;
        public int[][] matrix;
        public List<LoadReturnData> win;
        public bool freeGame;
        public int[] freeGameData;
        public bool bonus;
    }

    public class LoadGameSettingData
    {
        public LoadGameData gameDataObject;
        public LoadOutcomeData outcomeData;
    }

    public class LoadFreeSpinData
    {
        public string winType;
        public int symbol;
        public int count;
        public int amount;
        public int[][] symbolPos;
    }

    List<LoadFreeSpinData> checkForFreeSpinFound(int[][] _matrix)
    {
        bool freeSpinFound = false;
        int totalScatterSymbols = 0;
        int[][] symbolsPosition = new int[_matrix[0].Length][];
        //for (int col = 0; col < _matrix[0].Length; col++)
        //{
        //    for (int row = 0; row < _matrix.Length; row++)
        //    {
        //        if (_matrix[row][col] == scatterSymbol)
        //        {
        //            totalScatterSymbols++;
        //            symbolsPosition[col][symbolsPosition[col].Length] = row;
        //        }
        //    }
        //}
        for (int i = 0; i < _matrix.Length; i++)
        {
            for (int j = 0; j < _matrix[i].Length; j++)
            {
                if (_matrix[i][j] == scatterSymbol)
                {
                    totalScatterSymbols++;
                }
            }
        }
        print(totalScatterSymbols);

            if (totalScatterSymbols == 3 || totalScatterSymbols == 2)
        {
            freeSpinFound = true;
        }

        List<LoadFreeSpinData> returnData = new List<LoadFreeSpinData>();
        if (freeSpinFound != false)
        {
            LoadFreeSpinData loadData = new LoadFreeSpinData();
            loadData.winType = "scatter";
            loadData.symbol = scatterSymbol;
            loadData.count = totalScatterSymbols;
            loadData.amount = 0;
            loadData.symbolPos = symbolsPosition;
            returnData.Add(loadData);
        }
        else
        {
            returnData.Clear();
        }

        return returnData;
    }


    LoadGameSettingData freeGameSpin(int _betAmount, int[] _reelsStops)
    {
        print("free spin bet amount : "+ _betAmount);
        bet = _betAmount * minBet;
        myTotalBet = bet;
        Gameui.SetTotalBet(bet);
        string gameMode = "freeGame";
        List<LoadReturnData> winsFound = checkWin(gameMode, matrix);
        ////////End of Creating the FreeGame outcomes///////

        //////////Calculating totalWinFound in spin///////////
        int tempTotalWinInSpin = 0;
        for (int i = 0; i < winsFound.Count; i++)
        {
            tempTotalWinInSpin += winsFound[i].amount;
        }


        LoadGameData gamedataObject = new LoadGameData();
        gamedataObject.totalFreeSpins = 0;
        gamedataObject.freeSpinsLeft = 0;
        gamedataObject.accumulatedLineWin = tempTotalWinInSpin;

        LoadOutcomeData outcomeDataObject = new LoadOutcomeData();
        outcomeDataObject.type = gameMode;
        outcomeDataObject.matrix = matrix;
        outcomeDataObject.win = winsFound;

        LoadGameSettingData gameSettingsObject = new LoadGameSettingData();
        gameSettingsObject.gameDataObject = gamedataObject;
        gameSettingsObject.outcomeData = outcomeDataObject;
        return gameSettingsObject;

    }


    public class LoadCheckBonusData
    {
        public string winType;
        public int symbol;
        public int count;
        public int amount;
        public int[][] symbolPos;
    }

    List<LoadCheckBonusData> checkForBonusFound(int[][] _matrix)
    {
        bool bonusFound = false;
        int totalBonusSymbols = 0;
        int[][] symbolsPosition = new int[matrix[0].Length][];
        //for (int col = 0; col < _matrix[0].Length; col++)
        //{
        //    for (int row = 0; row < _matrix.Length; row++)
        //    {
        //        if (_matrix[row][col] == bonusSymbol)
        //        {
        //            totalBonusSymbols++;
        //            //symbolsPosition[col][symbolsPosition[col].Length] = row;
        //        }
        //    }
        //}

        for (int i = 0; i < matrix.Length; i++)
        {
            for (int j = 0; j < matrix[i].Length; j++)
            {
                if (_matrix[i][j] == bonusSymbol)
                {
                    totalBonusSymbols++;
                }
            }
        }

        print(totalBonusSymbols);

        if (totalBonusSymbols == 3 || totalBonusSymbols == 2)
        {
            bonusFound = true;
        }

        List<LoadCheckBonusData> returnData = new List<LoadCheckBonusData>();
        if (bonusFound != false)
        {
            LoadCheckBonusData loadDataObject = new LoadCheckBonusData();
            loadDataObject.winType = "bonus";
            loadDataObject.symbol = bonusSymbol;
            loadDataObject.count = totalBonusSymbols;
            loadDataObject.amount = 0;
            loadDataObject.symbolPos = symbolsPosition;
            returnData.Add(loadDataObject);
        }

        return returnData;

    }


    List<LoadReturnData> checkWin(string _gameType, int[][] matrix)
    {
        List<LoadReturnData> tempWinData = new List<LoadReturnData>();
        //int[][] temp = new int[][]
        //{
        //    new int[] {matrix[0][0] , matrix [1][0] , matrix[2][0]  },
        //    new int[] {matrix[0][1],matrix[1][1],matrix[2][1]},
        //    new int[]{matrix[0][2],matrix[1][2],matrix[2][2]},
        //    new int[]{matrix[0][3],matrix[1][3],matrix[2][3]},
        //    new int[]{matrix[0][4],matrix[1][4],matrix[2][4]}
        //};

        tempWinData = getWinWays(matrix, _gameType);

        //if (_gameType == "ways")
        //{
            
        //}
        return tempWinData;
    }

    public class LoadReturnData
    {
        public string winType;
        public int symbol;
        public int count;
        public int amount;
    }
    List<LoadReturnData> returnData = new List<LoadReturnData>();
    List<LoadReturnData> getWinWays(int[][] matrix, string _gameType)
    {
        int totalWinsFound = 0;
        int winCount = 0;
        int trgSymbol = 0;
        int total_win_amount = 0;
        for (int row = 0; row < matrix[0].Length; row++)
        {
            trgSymbol = matrix[0][row];
            if (trgSymbol == wild1 || trgSymbol == scatterSymbol || trgSymbol == bonusSymbol)
            {
                //Do Nothing
            }
            else
            {
                int symbolCountInCol2 = 0;
                for (int col2 = 0; col2 < matrix[1].Length; col2++)
                {
                    if (matrix[1][col2] == trgSymbol || matrix[1][col2] == wild1)
                    {
                        symbolCountInCol2++;
                    }
                }
                int symbolCountInCol3 = 0;
                for (int col3 = 0; col3 < matrix[2].Length; col3++)
                {
                    if (matrix[2][col3] == trgSymbol || matrix[2][col3] == wild1)
                    {
                        symbolCountInCol3++;
                    }
                }

                if (symbolCountInCol2 != 0 && symbolCountInCol3 != 0)
                {
                    //End of win of 3
                    winCount = 3;
                    totalWinsFound = symbolCountInCol2 * symbolCountInCol3;
                }
                else if (symbolCountInCol2 != 0)
                {
                    //End of win of 2
                    winCount = 2;
                    totalWinsFound = symbolCountInCol2;
                }
                if (winCount == 0)
                {
                    continue;
                }
                //print("totalWinsFound : " + totalWinsFound);
                //print("trgSymbol : " + trgSymbol);
                //print("winCount : " + winCount);
                int symbol_win_amount = getWinValue(winCount, trgSymbol) * totalWinsFound;
                total_win_amount += symbol_win_amount;
                LoadReturnData returndataobject = new LoadReturnData();
                returndataobject.winType = "ways";
                returndataobject.symbol = trgSymbol;
                returndataobject.count = winCount;
                returndataobject.amount = symbol_win_amount;
                returnData.Add(returndataobject);
            }//End             
        }//End for
        if (total_win_amount == 0)
        {
            returnData = null;
        }
        return returnData;
    }

    int getWinValue(int _multiplier, int _symbol)
    {
        //print("_multiplier" + _multiplier);
        //print("_symbol" + _symbol);
        int[][] paytable = new int[][]
        {
            new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0},
            new int[]{0,50,12,12,10,10,10,10,5,5,2,2},
            new int[]{0,75,30,30,25,25, 25, 25, 15, 15, 5, 5 }
        };
        return paytable[_multiplier - 1][_symbol];
    }

    int[][] CreateMatrix(int[][] _reels, int[] _reelsStops)
    {
        int[] random = _reelsStops;
        ////Generating Matrix
        int[][] matrix = new int[3][];
        int[] rowNumber = new int[] { 16, 16, 8 };

        //putting values in matrix
        for (int mat_col = 0; mat_col < 3; mat_col++)
        {
            int _numRow = rowNumber[mat_col];
            matrix[mat_col] = new int[_numRow];
            for (int mat_row = 0; mat_row < _numRow; mat_row++)
            {
                matrix[mat_col][mat_row] = _reels[mat_col][random[mat_col]];
                random[mat_col]++;
                if (random[mat_col] > _reels[mat_col].Length - 1)
                {
                    random[mat_col] = 0;
                }//End of if
            }//End of mat_row
        }//End of mat_col
        return matrix;
    }

}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BetPlaceItemList : MonoBehaviour {

    public int amount;
    public Button itemButton;

    void OnEnable()
    {
        if(itemButton != null)
            itemButton.onClick.AddListener(OnClick);
    }


    void OnDisable()
    {
        if (itemButton != null)
            itemButton.onClick.RemoveListener(OnClick);
    }

    void OnClick()
    {
        Gameui.betAmount = amount;
        Gameui.SetTotalBet(amount * 500);

    }
}

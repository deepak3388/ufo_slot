using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using SimpleJSON;
using UnityEngine.UI;
using System.Text.RegularExpressions;
using System.Linq;
using UnityEngine.SceneManagement;
using System.Text;
using System.IO;
/// <summary>
/// This monobehaviour is only required for running all the coroutines. Since we need a GameObject for that so it's instance object takes all the load.
/// </summary>
public class CommonMono : MonoBehaviour
{
	static CommonMono _instance;

	public static CommonMono instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = InstantiateNewObject();
			}
			else if (!_instance.gameObject.activeSelf)
			{
				_instance = InstantiateNewObject();
			}
			return _instance;
		} 
	}

	static CommonMono InstantiateNewObject()
	{
		GameObject go = new GameObject();
		go.name = "CommonMono";
		go.AddComponent<CommonMono>();
		return go.GetComponent<CommonMono>();
	}
    
}

public enum Flag
{
NotSet,
	True,
	False

}

/// <summary>
/// Extension methods extending default classes' functionality
/// </summary>
public static class Extensions
{
	//Array Extensions
	public static int[] SubArray(this int[] origArray, int subsetStart, int subsetEnd = 999999)
	{
		return ExtensionHelpers<int>.SubArray(origArray, subsetStart, subsetEnd);
	}

	public static char[] SubArray(this char[] origArray, int subsetStart, int subsetEnd = 999999)
	{
		return ExtensionHelpers<char>.SubArray(origArray, subsetStart, subsetEnd);
	}

	public static string[] SubArray(this string[] origArray, int subsetStart, int subsetEnd = 999999)
	{
		return ExtensionHelpers<string>.SubArray(origArray, subsetStart, subsetEnd);
	}

	//numbers extensions
	public static string ToFormattedString(this double num, bool useCommas = false, bool useK = false)
	{
		return FormattedString.FromDouble(num, useCommas, useK);
	}

	public static string ToFormattedString(this int num, bool useCommas = false, bool useK = false)
	{
		return FormattedString.FromDouble(num, useCommas, useK);
	}

	public static string ToFormattedString(this float num, bool useCommas = false, bool useK = false)
	{
		return FormattedString.FromDouble(num, useCommas, useK);
	}

	//Color Extensions
	public static string ToHex(this Color color)
	{ 
		Color32 color32 = color;
		return "#" + color32.r.ToString("X2") + color32.g.ToString("X2") + color32.b.ToString("X2");
	}

	public static int RoundTo(this int val, int roundTo)
	{
		return ((int)(val / roundTo)) * roundTo;
	}

	public static string GetDump(this IEnumerable collection, char separator = '\n')
	{
		StringBuilder sb = new StringBuilder();
		IEnumerator e = collection.GetEnumerator();
		e.Reset();
		while (e.MoveNext())
		{
			if (sb.Length > 0)
				sb.Append(separator);
			sb.Append(e.Current.ToString());
		}
		return sb.ToString();
	}

	/// <summary>
	/// Returns transparent rgb of current color
	/// Optionally provide opacity for controlled transparency
	/// </summary>
	/// <param name="color">original color</param>
	/// <param name="opacity">opacity of color 0-1</param>
	/// <returns></returns>
	public static Color transparent(this Color color, int transparency = 1)
	{
		return new Color(color.r, color.g, color.b, 1 - transparency);
	}

	//Transform extensions
	public static Transform Duplicate(this Transform transformToDuplicate)
	{
		return GameObject.Instantiate(transformToDuplicate, transformToDuplicate.parent) as Transform;
	}

	public static Transform Duplicate(this Transform transformToDuplicate, Transform parent)
	{
		return GameObject.Instantiate(transformToDuplicate, parent) as Transform;
	}

	public static void ClearChild(this Transform transformToClearChildOf)
	{
		List<GameObject> children = new List<GameObject>();

		for (int i = 0; i < transformToClearChildOf.childCount; i++)
			children.Add(transformToClearChildOf.GetChild(i).gameObject);

		for (int i = 0; i < children.Count; i++)
			GameObject.Destroy(children[i]);
	}

	public static void ClearChildren(this Transform transformToClearChildrenOf)
	{
		List<GameObject> children = new List<GameObject>();

		for (int i = 0; i < transformToClearChildrenOf.childCount; i++)
			children.Add(transformToClearChildrenOf.GetChild(i).gameObject);

		for (int i = 0; i < children.Count; i++)
			GameObject.Destroy(children[i]);
	}
}

/// <summary>
/// To overcome extension methods limitations, better used by Extension methods exclusively, not you!
/// </summary>
public static class ExtensionHelpers<T>
{
	//Array Extensions
	public static T[] SubArray(T[] origArray, int subsetStart, int subsetEnd)
	{
		if (subsetStart >= origArray.Length || subsetEnd < 0)
			return new T[] { };

		subsetStart = Mathf.Clamp(subsetStart, 0, origArray.Length - 1);
		subsetEnd = Mathf.Clamp(subsetEnd, 0, origArray.Length - 1);
		T[] subset = new T[Mathf.Clamp(subsetEnd - subsetStart + 1, 0, origArray.Length)];
		//Debug.Log("l: " + origArray.Length + " ss: " + subsetStart + " se: " + subsetEnd + " sl: " + subset.Length);

		for (int i = 0; i < subset.Length; i++)
		{ 
			subset[i] = origArray[i + subsetStart]; 
		}

		return subset;
	}
}

/// <summary>
/// Perform delayed actions. Explore it's functions to discover what can be done.
/// </summary>
public class Delayed
{

	#region static methods

	/// <summary>
	/// Pass a function of no parameters here with desired delay.
	/// <para>Remember while passing lambda expressions or anonymous functions that they may loose references to some variables inside them as those variables can change in meantime</para>
	/// </summary>
	/// <param name="function"> function of void return type</param>
	/// <param name="delay">desired delay</param>
	/// <returns></returns>
	public static Action Function(System.Action function, float delay)
	{
		return new Action(function, delay);
	}

	/// <summary>
	/// Pass a function which takes one parameter here with desired delay.
	/// <para>Remember while passing lambda expressions or anonymous functions that they may loose references to some variables inside them as those variables can change in meantime</para>
	/// </summary>
	/// <typeparam name="T">type of parameter(if you want to define)</typeparam>
	/// <param name="function">function that takes one parameter of any type</param>
	/// <param name="param">value of that parameter</param>
	/// <param name="delay">desired delay</param>
	/// <returns></returns>
	public static Action<T> Function<T>(System.Action<T> function, T param, float delay)
	{
		return new Action<T>(function, param, delay); 
	}

	/// <summary>
	/// I don't think you need any help if you're even trying to this
	/// </summary> 
	public static Action<T1, T2> Function<T1, T2>(System.Action<T1, T2> function, T1 param1, T2 param2, float delay)
	{
		return new Action<T1, T2>(function, param1, param2, delay); 
	}

	public static Action<T1, T2, T3> Function<T1, T2, T3>(System.Action<T1, T2, T3> function, T1 param1, T2 param2, T3 param3, float delay)
	{
		return new Action<T1, T2, T3>(function, param1, param2, param3, delay);
	}

	public static Action<T1, T2, T3, T4> Function<T1, T2, T3, T4>(System.Action<T1, T2, T3, T4> function, T1 param1, T2 param2, T3 param3, T4 param4, float delay)
	{
		return new Action<T1, T2, T3, T4>(function, param1, param2, param3, param4, delay);
	}
	#endregion


	public class Action
	{
		Coroutine cr = null;
		System.Action function;
		float delay;

		public Action(System.Action _function, float _delay)
		{
			function = _function;
			delay = _delay;
			Invoke();
		}

		public void Invoke()
		{
			cr = CommonMono.instance.StartCoroutine(Function_c());
		}

		public void CancelInvoke()
		{
			if (cr != null)
				CommonMono.instance.StopCoroutine(cr);
		}

		IEnumerator Function_c()
		{
			if (delay > 0)
				yield return new WaitForSeconds(delay);
			function();
			cr = null;
		}
	}

	public class Action<T1>
	{
		Coroutine cr = null;
		System.Action<T1> function;
		T1 param;
		float delay;

		public Action(System.Action<T1> _function, T1 _param, float _delay)
		{
			function = _function;
			param = _param;
			delay = _delay;
			Invoke();
		}

		public void Invoke()
		{
			cr = CommonMono.instance.StartCoroutine(Function_c());
		}

		public void CancelInvoke()
		{
			if (cr != null)
				CommonMono.instance.StopCoroutine(cr);
		}

		IEnumerator Function_c()
		{
			if (delay > 0)
				yield return new WaitForSeconds(delay);
			function(param);
			cr = null;
		}
	}

	public class Action<T1, T2>
	{
		Coroutine cr = null;
		System.Action<T1, T2> function;
		T1 param1;
		T2 param2;
		float delay;

		public Action(System.Action<T1, T2> _function, T1 _param1, T2 _param2, float _delay)
		{
			function = _function;
			param1 = _param1;
			param2 = _param2;
			delay = _delay;
			Invoke();
		}

		public void Invoke()
		{
			cr = CommonMono.instance.StartCoroutine(Function_c());
		}

		public void CancelInvoke()
		{
			if (cr != null)
				CommonMono.instance.StopCoroutine(cr);
		}

		IEnumerator Function_c()
		{
			if (delay > 0)
				yield return new WaitForSeconds(delay);
			function(param1, param2);
			cr = null;
		}
	}

	public class Action<T1, T2, T3>
	{
		Coroutine cr = null;
		System.Action<T1, T2, T3> function;
		T1 param1;
		T2 param2;
		T3 param3;
		float delay;

		public Action(System.Action<T1, T2, T3> _function, T1 _param1, T2 _param2, T3 _param3, float _delay)
		{
			function = _function;
			param1 = _param1;
			param2 = _param2;
			param3 = _param3;
			delay = _delay;
			Invoke();
		}

		public void Invoke()
		{
			cr = CommonMono.instance.StartCoroutine(Function_c());
		}

		public void CancelInvoke()
		{
			if (cr != null) CommonMono.instance.StopCoroutine(cr);
		}

		IEnumerator Function_c()
		{
			if (delay > 0) yield return new WaitForSeconds(delay);
			function(param1, param2, param3);
			cr = null;
		}
	}

	public class Action<T1, T2, T3, T4>
	{
		Coroutine cr = null;
		System.Action<T1, T2, T3, T4> function;
		T1 param1;
		T2 param2;
		T3 param3;
		T4 param4;
		float delay;

		public Action(System.Action<T1, T2, T3, T4> _function, T1 _param1, T2 _param2, T3 _param3, T4 _param4, float _delay)
		{
			function = _function;
			param1 = _param1;
			param2 = _param2;
			param3 = _param3;
			param4 = _param4;
			delay = _delay;
			Invoke();
		}

		public void Invoke()
		{
			cr = CommonMono.instance.StartCoroutine(Function_c());
		}

		public void CancelInvoke()
		{
			if (cr != null) CommonMono.instance.StopCoroutine(cr);
		}

		IEnumerator Function_c()
		{
			if (delay > 0) yield return new WaitForSeconds(delay);
			function(param1, param2, param3, param4);
			cr = null;
		}
	}

}

/// <summary>
/// Animate things here. Explore it's child classes to discover what can be done.
/// </summary>
public class Interpolate
{
	/// <summary>
	/// Scales a transform from one position to another.
	/// <para>You create a new class for each new interpolation(movement will automatically start), save it's reference if you later want to stop the movement.</para>
	/// </summary>
	public class Scale
	{
		Coroutine cr;

		public bool isRunning
		{
			get
			{
				return cr != null;
			}
		}

		public 
		AnimationCurve curve = new AnimationCurve(new Keyframe[] { new Keyframe(0, 0), new Keyframe(1, 1) });

		/// <summary>
		/// Scales a transform from one vector3 to another.
		/// <para>You create a new class for each new interpolation(scaling will automatically start), save it's reference if you later want to stop or modify the animation.</para>
		/// </summary>
		/// <param name="transform">transform to scale</param>
		/// <param name="from">starting scale</param>
		/// <param name="to">ending scale</param>
		/// <param name="duration">time duration to reach the final scale</param> 
		public Scale(Transform transform, Vector3 from, Vector3 to, float duration, AnimationCurve animCurve = null)
		{
			//if (Vector3.Distance(from, to) < .01f) { Debug.Log("wasted"); return; }
			if(animCurve!=null) curve = animCurve;
			cr = CommonMono.instance.StartCoroutine(Interpolation(transform, from, to, duration));
		}

		public void Stop()
		{
			if (cr != null)
				CommonMono.instance.StopCoroutine(cr);
		}

		IEnumerator Interpolation(Transform transform, Vector3 from, Vector3 to, float duration)
		{
			float t = 0;
			float deltaTime = 0, realTimeLastFrame = Time.realtimeSinceStartup;

			if (transform != null) {
				while (t < duration) {
					deltaTime = Time.realtimeSinceStartup - realTimeLastFrame;
					realTimeLastFrame = Time.realtimeSinceStartup;
					t += deltaTime;
					if (transform != null) {
						transform.localScale = Vector3.Lerp (from, to, curve.Evaluate (t / duration * 1f));
					}
					yield return null;
				}
				if (transform != null) {
					transform.localScale = to; 
				}
				cr = null;
			}
		}
	}

	/// <summary>
	/// Moves a transform from one position to another.
	/// <para>You create a new class for each new interpolation(movement will automatically start), save it's reference if you later want to stop the movement.</para>
	/// </summary>
	public class Position
	{
		Coroutine cr;

		public bool isRunning
		{
			get
			{
				return cr != null;
			}
		}

		public  AnimationCurve curve = new AnimationCurve(new Keyframe[] { new Keyframe(0, 0), new Keyframe(1, 1) });

		/// <summary>
		/// Moves a transform from one position to another.
		/// <para>You create a new class for each new interpolation(movement will automatically start), save it's reference if you later want to stop the movement.</para>
		/// </summary>
		/// <param name="transform">transform to move</param>
		/// <param name="from">starting position</param>
		/// <param name="to">ending position</param>
		/// <param name="duration">time duration to reach the final destination</param>
		/// <param name="local">to modify localPosition or Position instead</param>
		public Position(Transform transform, Vector3 from, Vector3 to, float duration, bool local = false,AnimationCurve animCurve = null)
		{
			if(animCurve!=null) curve = animCurve;
			//if (Vector3.Distance(from, to) < .01f) { Debug.Log("wasted"); return; }
			cr = CommonMono.instance.StartCoroutine(Interpolation(transform, from, to, duration, local));
		}

		public void Stop()
		{
			if (cr != null)
				CommonMono.instance.StopCoroutine(cr);
		}

		IEnumerator Interpolation(Transform transform, Vector3 from, Vector3 to, float duration, bool local = false)
		{
			float t = 0;
			float deltaTime = 0, realTimeLastFrame = Time.realtimeSinceStartup;
//			if (transform != null) {
				while (t < duration) {
					deltaTime = Time.realtimeSinceStartup - realTimeLastFrame;
					realTimeLastFrame = Time.realtimeSinceStartup;
					t += deltaTime;
					if (local) {
//						if (transform != null && from != null && to != null) {
							transform.localPosition = Vector3.Lerp (from, to, curve.Evaluate (t / duration * 1f));
//						}
					} else {
//						if (transform != null && from != null && to != null) {
							transform.position = Vector3.Lerp (from, to, curve.Evaluate (t / duration * 1f));
//						}
					}
//				}
					yield return null;

			}
			if (local) {
//				if (transform != null  && to != null) {
					transform.localPosition = to;
//				}
			} else {
//				if (transform != null  && to != null) {
					transform.position = to;
//				}
			}
			cr = null;
		}
	}

	/// <summary>
	/// Rotates a transform from one eulerAngles to another.
	/// <para>You create a new class for each new interpolation(rotation will automatically start), save it's reference if you later want to stop the rotation.</para>
	/// </summary>
	public class Eulers
	{
		Coroutine cr;

		public bool isRunning
		{
			get
			{
				return cr != null;
			}
		}

		public  AnimationCurve curve = new AnimationCurve(new Keyframe[] { new Keyframe(0, 0), new Keyframe(1, 1) });


		/// <summary>
		/// Rotates a transform from one eulerAngles to another.
		/// <para>You create a new class for each new interpolation(rotation will automatically start), save it's reference if you later want to stop the rotation.</para>
		/// </summary>
		/// <param name="transform">target transform to rotate</param>
		/// <param name="from">starting eulerAngles</param>
		/// <param name="to">final eulerAngles</param>
		/// <param name="duration">time duration to rotate to final eulerAngles</param>
		/// <param name="local">to modify localEulerAngles or eulerAngles</param>
		public Eulers(Transform transform, Vector3 from, Vector3 to, float duration, bool local = false)
		{
			cr = CommonMono.instance.StartCoroutine(Interpolation(transform, from, to, duration, local));
		}

		public void Stop()
		{
			if (cr != null)
				CommonMono.instance.StopCoroutine(cr);
		}

		IEnumerator Interpolation(Transform transform, Vector3 from, Vector3 to, float duration, bool local = false)
		{
			
			float t = 0;
			float deltaTime = 0, realTimeLastFrame = Time.realtimeSinceStartup;
			while (t < duration) {
//				if (transform != null && from != null && to != null) {
				deltaTime = Time.realtimeSinceStartup - realTimeLastFrame;
				realTimeLastFrame = Time.realtimeSinceStartup;
				t += deltaTime;
				if (local) {
//						if (transform != null && from != null && to != null) {
					transform.localEulerAngles = Vector3.Lerp (from, to, curve.Evaluate (t / duration * 1f));
//						}
				} else {
//						if (transform != null && from != null && to != null) {
					transform.eulerAngles = Vector3.Lerp (from, to, curve.Evaluate (t / duration * 1f));
//						}
				}

				yield return null;
			}
//			}
			if (local) {
//				if (transform != null && to != null) {
					transform.localEulerAngles = to;
//				}
			} else {
//				if (transform != null && to != null) {
					transform.eulerAngles = to;
//				}
			}
			cr = null;
		}
	}



	/// <summary>
	/// Changes color of a UI graphic component.
	/// <para>You create a new class for each new interpolation(animation will automatically start), save it's reference if you later want to stop the animation.</para>
	/// </summary>
	public class UIGraphicColor
	{
		Coroutine cr;

		public bool isRunning
		{
			get
			{
				return cr != null;
			}
		}

		public static AnimationCurve curve = new AnimationCurve(new Keyframe[] { new Keyframe(0, 0), new Keyframe(1, 1) });


		/// <summary>
		/// Changes color of an UI graphic from one color to another.
		/// <para>You create a new class for each new interpolation(animation will automatically start), save it's reference if you later want to stop the animation.</para>
		/// </summary>
		/// <param name="transform">target image</param>
		/// <param name="from">starting color</param>
		/// <param name="to">final color</param>
		/// <param name="duration">time duration to change to final color</param> 
		public UIGraphicColor(MaskableGraphic graphic, Color from, Color to, float duration)
		{
			cr = CommonMono.instance.StartCoroutine(Interpolation(graphic, from, to, duration));
		}

		public void Stop()
		{
			if (cr != null)
				CommonMono.instance.StopCoroutine(cr);
		}

		IEnumerator Interpolation(MaskableGraphic graphic, Color from, Color to, float duration)
		{
			float t = 0;
			float deltaTime = 0, realTimeLastFrame = Time.realtimeSinceStartup;
			while (t < duration)
			{
				deltaTime = Time.realtimeSinceStartup - realTimeLastFrame;
				realTimeLastFrame = Time.realtimeSinceStartup;
				t += deltaTime;
				graphic.color = Color.Lerp(from, to, curve.Evaluate(t / duration * 1f));
				yield return null;
			}
			graphic.color = to;
			cr = null;
		}
	}
}

public class DownloadImage
{
	public static Dictionary<string, Texture2D> downloadedImages = new Dictionary<string, Texture2D>();

	public class ToMaterial
	{
		public Texture2D tex;
		public Material _mat;

		public ToMaterial(string url, Material mat)
		{
			_mat = mat;
			if (downloadedImages.ContainsKey(url))
			{
				tex = downloadedImages[url];
				Load();
			}
			else
				CommonMono.instance.StartCoroutine(Loading(url));
		}

		IEnumerator Loading(string url)
		{
			WWW req = new WWW(url);
			yield return req;
			if (string.IsNullOrEmpty(req.error))
			{
				req.LoadImageIntoTexture(tex);
				Load();
			}
		}

		void Load()
		{
			_mat.mainTexture = tex;
		}
	}

	public class ToUIImage
	{
		public Texture2D tex = new Texture2D(1, 1);
		UnityEngine.UI.Image uiImg;

		public ToUIImage(string _url, UnityEngine.UI.Image _uiImg)
		{
			uiImg = _uiImg;
			//if (downloadedImages.ContainsKey(_url))
			//{
			//    tex = downloadedImages[_url];
			//    Load();
			//}
			//else
			CommonMono.instance.StartCoroutine(Loading(_url));
		}

		IEnumerator Loading(string url)
		{
			WWW req = new WWW(url);
			yield return req;
			if (string.IsNullOrEmpty(req.error))
			{
				req.LoadImageIntoTexture(tex);
				//if (downloadedImages.Count > 20) downloadedImages.Clear();
				//downloadedImages.Add(url, tex);
				Load();
			}
		}

		void Load()
		{
			uiImg.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f));
			if (!uiImg.enabled)
				uiImg.enabled = true;
		}
	}
}

public class FormattedString
{
	public static string FromDouble(double number, bool useCommas = false, bool useK = false)
	{
		if (useK)
		{
			if (number > 999)
				number = number / 1000;
			else
				useK = false;
		}

		string s = "";

		if (useCommas)
		{
			if (number % 1 == 0)
				s = number.ToString("N0");
			else
				s = number.ToString("N1");
		}
		else
		{
			if (number % 1 == 0)
				s = number.ToString("F0");
			else
				s = number.ToString("F1");
		}

		if (useK)
			s += "K";

		return s;
	}

	public static string ToSentenceCase(string text)
	{
		string txt = "";

		//using the brain of a 5th standard (source: tera bhai)=>
		//string[] sentences = text.Split('.');
		//bool upperCharProcessed = false;
		//for (int i = 0; i < sentences.Length; i++)
		//{
		//    upperCharProcessed = false;
		//    for (int s = 0; s < sentences[i].Length; s++)
		//    {
		//        if (!upperCharProcessed && char.IsLetter(sentences[i][s]))
		//        {
		//            txt += sentences[i][s].ToString().ToUpper();
		//            upperCharProcessed = true;
		//        }
		//        else txt += sentences[i][s].ToString().ToLower();
		//    }
		//    if (i < sentences.Length - 1) txt += '.';
		//}


		//using regex, much powerful (source: stack overflow) =>
		string lowerCase = text.ToLower();
		Regex r = new Regex(@"(^[a-z])|[?!.:]\s+(.)", RegexOptions.ExplicitCapture);
		txt = r.Replace(lowerCase, s => s.Value.ToUpper()); 

		return txt;
	}
}

public static class Validation
{
	public static bool HasEmptyChars(this string s)
	{ 
		return GetRegexMatchCount(s, @"[ \n\f\b]") > 0;
	}

	public static bool HasSpecialChar(this string s)
	{
		return GetRegexMatchCount(s, @"[!#$%&()*+,-/:;<>?@[]{}_|~]") > 0; 
	}

	public static bool HasUpperCaseAlphabet(this string s)
	{
		return GetRegexMatchCount(s, @"[A-Z]") > 0;
	}

	public static bool HasLowerCaseAlphabet(this string s)
	{
		return GetRegexMatchCount(s, @"[a-z]") > 0;
	}

	public static bool HasNumber(this string s)
	{
		return GetRegexMatchCount(s, @"[0-9]") > 0;
	}

	public static bool HasLengthBetween(this string s, int min, int max)
	{
		return s.Length >= min && s.Length <= max;
	}

	public static bool IsEmail(this string s)
	{ 
		return GetRegexMatchCount(s, @"\w+@\w{2,}\.\w{2,}") == 1; 
	}

	public static bool IsStrongPassword(this string s)
	{
		if (s.HasEmptyChars())
			return false;

		if (!s.HasLengthBetween(8, 15))
			return false;

		if (!s.HasUpperCaseAlphabet())
			return false;

		if (!s.HasLowerCaseAlphabet())
			return false;

		if (!s.HasNumber())
			return false;

		return true;
	}

	public static int GetRegexMatchCount(string s, string r)
	{
		Regex reg = new Regex(r, RegexOptions.ExplicitCapture);
		return reg.Matches(s).Count;
	}
}

/// <summary>
/// Finds components in the entire hirarchy under the passed transform
/// <para>Create a new instance of this class and define what component you want to look for, also pass the transform that is grand parent</para>
/// <para>later you can access all components from the list 'comps'</para>
/// </summary> 
public class ComponentsInChildren<T>
{
	public List<T> comps = new List<T>();

	/// <summary>
	/// Finds components in the entire hirarchy under the passed transform
	/// <para>Create a new instance of this class and define what component you want to look for, also pass the transform that is grand parent</para>
	/// <para>later you can access all components from the list 'comps'</para>
	/// </summary> 
	public ComponentsInChildren(Transform t)
	{
		comps.Clear();
		ProcessTransform(t); 
	}

	public void ProcessTransform(Transform tx)
	{
		for (int i = 0; i < tx.childCount; i++)
		{
			if (tx.GetChild(i).childCount > 0)
			{
				ProcessTransform(tx.GetChild(i));
			}
			T comp = tx.GetChild(i).GetComponent<T>();
			if (comp != null)
				comps.Add(comp);
		}
	}
}

public class RestAPI
{
	public class Get
	{
		string url;
		System.Action<string> successCallback;
		System.Action<string> failureCallback;

		public Get(string _url, System.Action<string> _successCallback = null, System.Action<string> _failureCallback = null)
		{
			url = _url; 
			successCallback = _successCallback;
			failureCallback = _failureCallback;
			CommonMono.instance.StartCoroutine(this.WWW_Text());
		}

		IEnumerator WWW_Text()
		{
			WWW req = new WWW(url);
			yield return req;
			if (successCallback != null)
			if (string.IsNullOrEmpty(req.error))
				successCallback(req.text);
			if (failureCallback != null)
			if (!string.IsNullOrEmpty(req.error))
				failureCallback(req.error);
		}
	}

	public class Post
	{
		string url;
		WWWForm form;
		System.Action<string> successCallback;
		System.Action<string> failureCallback;

		public Post(string _url, WWWForm _form, System.Action<string> _successCallback = null, System.Action<string> _failureCallback = null)
		{
			url = _url;
			form = _form;
			successCallback = _successCallback;
			failureCallback = _failureCallback;
			CommonMono.instance.StartCoroutine(this.WWW_Text());
		}

		IEnumerator WWW_Text()
		{
			WWW req = new WWW(url, form); 
			yield return req;
			if (successCallback != null)
			if (string.IsNullOrEmpty(req.error))
				successCallback(req.text);
			if (failureCallback != null)
			if (!string.IsNullOrEmpty(req.error))
				failureCallback(req.error);
		}
	}

	public class Post<T>
	{
		string url;
		WWWForm form;
		System.Action<T> successCallback;
		System.Action<string> failureCallback;

		public Post(string _url, WWWForm _form, System.Action<T> _successCallback = null, System.Action<string> _failureCallback = null)
		{
			url = _url;
			form = _form;
			successCallback = _successCallback;
			failureCallback = _failureCallback;
			CommonMono.instance.StartCoroutine(this.WWW_Text());
		}

		IEnumerator WWW_Text()
		{
			WWW req = new WWW(url, form);
			yield return req;
			T response = JsonUtility.FromJson<T>(req.text);
			if (successCallback != null)
			if (string.IsNullOrEmpty(req.error))
				successCallback(response);
			if (failureCallback != null)
			if (!string.IsNullOrEmpty(req.error))
				failureCallback(req.error);
		}
	}

	static Dictionary<string, string> savedStrings = new Dictionary<string, string>();

	public class SaveStringValue
	{
		string url;
		WWWForm form;
		string keyToAccess, keyInResponse;

		public SaveStringValue(string _url, WWWForm _form, string _keyToAccess, string _keyInResponse)
		{
			if (savedStrings.ContainsKey(_keyToAccess))
				return;  
			url = _url;
			form = _form;
			keyToAccess = _keyToAccess;
			keyInResponse = _keyInResponse;
			savedStrings.Add(keyToAccess, "");
			CommonMono.instance.StartCoroutine(this.WWW_StringVal());
		}

		IEnumerator WWW_StringVal()
		{
			WWW req = new WWW(url, form);
			yield return req;
			var response = JSON.Parse(req.text);
			savedStrings[keyToAccess] = response[keyInResponse].ToString().Replace("\"", string.Empty);
		}
	}

	public static string GetSavedStringValue(string _keyToAccess)
	{
		if (savedStrings.ContainsKey(_keyToAccess))
			return "";
		return savedStrings[_keyToAccess];
	}
}

public class Download
{ 
	public class Image
	{
		static Texture2D _emptyTexture;
		public static Texture2D emptyTexture
		{
			get{ 
				if (_emptyTexture == null)
				{
					_emptyTexture = new Texture2D(1, 1);
					_emptyTexture.SetPixel(0, 0, Color.black.transparent(1));
					_emptyTexture.Apply();
				}
				return _emptyTexture;
			}
		}

		public Texture2D tex = new Texture2D(1, 1);
		string webUrl = "";
		string localUrl = "";
		string localDir = "";

		UnityEngine.UI.Image uiImg;
		System.Action<Texture2D> successCallback;
		System.Action<string> failureCallback;

		void SetUrl (string val)
		{ 
			webUrl = val;
			localUrl = Application.persistentDataPath + val.Substring(val.IndexOf('/', 8)); 
			StringBuilder sb = new StringBuilder(localUrl);
			for (int i = localUrl.Length-1; i > 0; i--)
			{ 
				if (sb[i].Equals('/'))
				{
					sb.Remove(i, sb.Length - i);
					localDir = sb.ToString();
					break;
				}
			}
		} 


		public Image(string _url, UnityEngine.UI.Image _uiImg, bool cache = false, System.Action<Texture2D> _sucessCallback = null, System.Action<string> _failureCallback = null)
		{
			successCallback = _sucessCallback; 
			failureCallback = _failureCallback;
			uiImg = _uiImg;   
			SetUrl(_url);
			CommonMono.instance.StartCoroutine(Loading(cache));
			uiImg.sprite = Sprite.Create(emptyTexture, new Rect(0, 0, 1, 1), new Vector2(0.5f, 0.5f)); 
		} 

		public Image(string _url, bool cache, System.Action<Texture2D> _sucessCallback, System.Action<string> _failureCallback)
		{
			Debug.Log("Downloading Image: "+_url);
			successCallback = _sucessCallback;
			failureCallback = _failureCallback; 
			SetUrl(_url);
			CommonMono.instance.StartCoroutine(Loading(cache));
		}

		IEnumerator Loading(bool cache)
		{
			bool local = false;
			if (cache && File.Exists(localUrl)) local = true; 

			WWW req = new WWW(local ? ("file:///"+localUrl) : webUrl);
			//            UnityWebRequest req = new UnityWebRequest(local ? ("file:///"+localUrl) : webUrl);
			yield return req;
			//            while (!req.isDone || !req.isError)
			//            {
			//                yield return null;
			//                Debug.Log(req.downloadProgress);
			//            }
			if (string.IsNullOrEmpty(req.error))
			{
				Debug.Log("Downloading Complete: " + (cache ? localUrl : webUrl));
				req.LoadImageIntoTexture(tex);
				//                tex = DownloadHandlerTexture.GetContent(req);

				if (uiImg != null)
					LoadImage(); 

				if (successCallback != null)
					successCallback(tex);

				if (!local && cache)
				{
					if (!Directory.Exists(localDir))
					{
						Directory.CreateDirectory(localDir);
					}
					File.WriteAllBytes(localUrl, tex.EncodeToPNG()); 
				}
			}
			else
			{
				Debug.Log("Downloading Failed: " + webUrl);
				//                Debug.Log(req.error);
				failureCallback(req.error);
			}
		}

		void LoadImage()
		{
			uiImg.sprite = Sprite.Create(tex, new Rect(0, 0, tex.width, tex.height), new Vector2(0.5f, 0.5f)); 
		} 
	}
}

public static class NumbersExt
{
	public static double Lerp(double a, double z, float f)
	{
		return a + (z - a) * f;
	}
}


[System.Serializable]
public class PositionRefrence
{
	public Vector3 point;
	public Transform transform;

	public Vector3 value
	{
		get
		{
			if (transform == null)
			{
				return point;
			}
			else
			{
				return transform.position;
			}
		}
	}
}




public class LoadSceneProcess
{
	AsyncOperation op = null;
	public LoadSceneProcess(string sceneName)
	{
		Debug.Log("LoadSceneProcess: " + sceneName);
		op = SceneManager.LoadSceneAsync(sceneName, LoadSceneMode.Single);
	} 
	//    IEnumerator OpCheck()
	//    {
	//        yield return op;
	//    }
}




/// <summary>
/// You can use this in place of normal string if the value of string needs to pe displayed on several UI Text components. Use its 'value' variable to access its uh.. value
/// <para>It will appear as a dropdown in inspector where you can assign all Text components which will be updated automatically</para>
/// <para>Eg.: uString str = new uString();  str.value="hi";   string aRealString = str.value;</para>
/// </summary>
[System.Serializable]
public class uString
{
	[SerializeField]
	string m_value = "";

	public string value
	{
		get
		{
			return m_value;
		}
		set
		{
			m_value = value;
			if (textComps != null)
			{
				for (int i = 0; i < textComps.Length; i++)
				{
					if (textComps != null)
					{
						textComps[i].text = m_value;
					}
				}
			}
		}
	}

	public Text[] textComps = new Text[] { };

	public uString()
	{

	}
}

[System.Serializable]
public class uNumber
{
	[SerializeField]
	double m_value = 0;
	//public double m_value = 0;
	public double value
	{
		get
		{
			return Mathf.Clamp((float)m_value, (float)min, (float)max);
		}
		set
		{
			m_value = Mathf.Clamp((float)value, (float)min, (float)max);
			;
			if (textComps != null)
			{
				for (int i = 0; i < textComps.Length; i++)
				{
					if (textComps != null)
					{
						textComps[i].text = FormattedString.FromDouble(m_value, formatWithCommas, formatWithK);
					}
				}
			}
		}
	}

	public double min = -9999999;
	public double max = 9999999;

	public bool formatWithCommas = false;
	public bool formatWithK = false;

	public Text[] textComps = new Text[] { };

	//Cant have them, serialized class
	//public uNumber(double _min = -Mathf.Infinity, double _max = Mathf.Infinity, bool _formatWithCommas = false, bool _formatWithK = false)
	//{
	//    min = _min;
	//    max = _max;
	//    formatWithCommas = _formatWithCommas;
	//    formatWithK = _formatWithK;
	//}
}
[System.Serializable]
public class Int2D
{
	public int x, y;

	public Int2D(int _x, int _y)
	{
		x = _x;
		y = _y;
	}
}
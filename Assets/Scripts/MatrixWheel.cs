﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MatrixWheel : MonoBehaviour {





    public static System.Action OnStartWheelRotation;
    public static System.Action OnStopWheelRotation;

    public AnimationCurve rotationCurve = AnimationCurve.Linear(0, 0, 1, 1);
    float elapsedRotationTime1 = 0;
    float totalRotationTime1_ = 8;

    public AnimationCurve rotationCurve2 = AnimationCurve.Linear(0, 0, 1, 1);
    float elapsedRotationTime2 = 0;
    float totalRotationTime2_ = 7;

    public AnimationCurve rotationCurve3 = AnimationCurve.Linear(0, 0, 1, 1);
    float elapsedRotationTime3 = 0;
    float totalRotationTime3_ = 6;

    public Transform matrixWheelChild1;
    public Transform matrixWhee2Child1;
    public Transform matrixWhee3Child1;

    public Transform wheel1;
    public Transform wheel2;
    public Transform wheel3;

    static MatrixWheel instance_;


    void OnEnable()
    {
        instance_ = this;
    }

    void OnDisable()
    {

    }

    public static void RotateChildren()
    {



        instance_.StopCoroutine("RotateChildrenMatrix1");
        instance_.StartCoroutine("RotateChildrenMatrix1");


        instance_.StopCoroutine("RotateChildrenMatrix2");
        instance_.StartCoroutine("RotateChildrenMatrix2");


        instance_.StopCoroutine("RotateChildrenMatrix3");
        instance_.StartCoroutine("RotateChildrenMatrix3");

        instance_.StopCoroutine("RotateParents_1");
        instance_.StartCoroutine("RotateParents_1");


        instance_.StopCoroutine("RotateParents_2");
        instance_.StartCoroutine("RotateParents_2");


        instance_.StopCoroutine("RotateParents_3");
        instance_.StartCoroutine("RotateParents_3");




    }

    IEnumerator RotateChildrenMatrix1()
    {
        if (OnStartWheelRotation != null)
        {
            OnStartWheelRotation();
        }

        
        elapsedRotationTime1 = 0;

        float r = ((int)totalRotationTime1_) * -360;
        float rt = 0;
        float lf = 0;
        float lta = 0;
        //Debug.Log("totalRotationTime1 : " + totalRotationTime1_);
        while (elapsedRotationTime1 < totalRotationTime1_)
        {
            lf = rotationCurve.Evaluate(elapsedRotationTime1 / totalRotationTime1_);
            rt = Mathf.Lerp(0, r, lf);
            matrixWheelChild1.localEulerAngles = new Vector3(0, 0, rt % 360);
            yield return null;
            elapsedRotationTime1 += Time.deltaTime;
            if (Mathf.Abs(matrixWheelChild1.localEulerAngles.z - lta) > 20)
            {
                lta = matrixWheelChild1.localEulerAngles.z;
            }
        }
        matrixWheelChild1.localEulerAngles = Vector3.zero;
        if (OnStopWheelRotation != null)
        {
            OnStopWheelRotation();
        }
    }


    IEnumerator RotateChildrenMatrix2()
    {

        elapsedRotationTime2 = 0;

        float r = ((int)totalRotationTime2_) * 360;
        float rt = 0;
        float lf = 0;
        float lta = 0;
        //Debug.Log("totalRotationTime1 : " + totalRotationTime2_);
        while (elapsedRotationTime2 < totalRotationTime2_)
        {
            lf = rotationCurve2.Evaluate(elapsedRotationTime2 / totalRotationTime2_);
            rt = Mathf.Lerp(0, r, lf);
            matrixWhee2Child1.localEulerAngles = new Vector3(0, 0, rt % 360);
            yield return null;
            elapsedRotationTime2 += Time.deltaTime;
            if (Mathf.Abs(matrixWhee2Child1.localEulerAngles.z - lta) > 20)
            {
                lta = matrixWhee2Child1.localEulerAngles.z;
            }
        }
        matrixWhee2Child1.localEulerAngles = Vector3.zero;
        yield return new WaitForSeconds(0.5f);
    }


    IEnumerator RotateChildrenMatrix3()
    {
        elapsedRotationTime3 = 0;
        float r = ((int)totalRotationTime3_) * -360;
        float rt = 0;
        float lf = 0;
        float lta = 0;
        //Debug.Log("totalRotationTime1 : " + totalRotationTime3_);
        while (elapsedRotationTime3 < totalRotationTime3_)
        {
            lf = rotationCurve3.Evaluate(elapsedRotationTime3 / totalRotationTime3_);
            rt = Mathf.Lerp(0, r, lf);
            matrixWhee3Child1.localEulerAngles = new Vector3(0, 0, rt % 360);
            yield return null;
            elapsedRotationTime3 += Time.deltaTime;
            if (Mathf.Abs(matrixWhee3Child1.localEulerAngles.z - lta) > 20)
            {
                lta = matrixWhee3Child1.localEulerAngles.z;
            }
        }
        matrixWhee3Child1.localEulerAngles = Vector3.zero;
        yield return new WaitForSeconds(0.5f);
    }


    IEnumerator RotateParents_1()
    {
        float t = 3.5f;
        //            Debug.Log(t);
        if (t < 0) t = 0;
        float te = 0;
        float r1t = 0, r2t = 0, r3t = 0;
        float lf = 0;

        while (te < t)
        {
            //                lf = rotationCurve.Evaluate(elapsedRotationTime3 / totalRotationTime3);
            lf = rotationCurve.Evaluate(te / t);
            r1t = Mathf.Lerp(360, 0f, lf);

            wheel1.localEulerAngles = new Vector3(0, 0, -r1t % 360);

            yield return null;
            te += Time.deltaTime;
        }


        wheel1.localEulerAngles = new Vector3(0, 0, 0);


    }

    IEnumerator RotateParents_2()
    {
        float t = 3f;
        //            Debug.Log(t);
        if (t < 0) t = 0;
        float te = 0;
        float r1t = 0, r2t = 0, r3t = 0;
        float lf = 0;

        while (te < t)
        {
            //                lf = rotationCurve.Evaluate(elapsedRotationTime3 / totalRotationTime3);
            lf = rotationCurve.Evaluate(te / t);
            r2t = Mathf.Lerp(360, 0f, lf);

            wheel2.localEulerAngles = new Vector3(0, 0, r2t % 360);

            yield return null;
            te += Time.deltaTime;
        }

        wheel2.localEulerAngles = new Vector3(0, 0, 0);


    }


    IEnumerator RotateParents_3()
    {
        float t = 2.5f;
        //            Debug.Log(t);
        if (t < 0) t = 0;
        float te = 0;
        float r1t = 0, r2t = 0, r3t = 0;
        float lf = 0;

        while (te < t)
        {
            //                lf = rotationCurve.Evaluate(elapsedRotationTime3 / totalRotationTime3);
            lf = rotationCurve.Evaluate(te / t);
            r3t = Mathf.Lerp(360, 0f, lf);

            wheel3.localEulerAngles = new Vector3(0, 0, -r3t % 360);

            yield return null;
            te += Time.deltaTime;
        }

        wheel3.localEulerAngles = new Vector3(0, 0, 0);

    }



}

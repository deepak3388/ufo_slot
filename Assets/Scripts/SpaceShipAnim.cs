﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceShipAnim : MonoBehaviour {

    static SpaceShipAnim instance_;

    public Transform mask;

    Interpolate.Scale scaleAnim;

    void OnEnable()
    {
        instance_ = this;
        mask.gameObject.SetActive(false);
    }

    void OnDisable()
    {

    }


	// Use this for initialization
	void Start () {
		
	}
	
    public static void OnSpaceAnim()
    {
        instance_.OnAnim();
    }

	// Update is called once per frame
	void Update () {
		
	}

    void OnAnim()
    {
        scaleAnim = new Interpolate.Scale(this.transform , Vector3.zero , Vector3.one , 1.0f);
        Delayed.Function(()=>
        {
            OnMaskAnim();
        } , 1.2f);
    }

    void OnMaskAnim()
    {
        mask.localScale = Vector3.zero;
        mask.gameObject.SetActive(true);
        scaleAnim = new Interpolate.Scale(mask, Vector3.zero, Vector3.one, 1.0f);
    }



}

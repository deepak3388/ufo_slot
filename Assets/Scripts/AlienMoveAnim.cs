﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AlienMoveAnim : MonoBehaviour
{

    Interpolate.Position posAnim;
    public Transform startPos;
    public Transform endPos;

    public GameObject obj;

    private void OnEnable()
    {
        posAnim = new Interpolate.Position(this.transform, startPos.position, endPos.position, 1.0f, false);
        Delayed.Function(()=>
        {
            if (obj != null)
                obj.SetActive(true);

            Delayed.Function(() => { 
            if (obj != null)
                obj.SetActive(false);
            } , 0.5f);
        } , 3.0f);
    }


    // Update is called once per frame
    void Update()
    {
        
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;  
using UnityEngine.UI;
public class OnRaycastCollision : MonoBehaviour
{
  
    public void TriggerFunctionOnRaycastHit()
    {

        this.GetComponent<Button>().onClick.Invoke();
    }
}

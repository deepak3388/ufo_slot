﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonClickInvoker : MonoBehaviour {


    // Use this for initialization
    public void InvokeButtonClick()
    {
        this.GetComponent<Button>().onClick.Invoke();
    }
}
